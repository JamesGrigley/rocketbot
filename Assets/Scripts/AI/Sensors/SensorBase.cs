﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;


public abstract class SensorBase : MonoBehaviour {

    public float senseRadius = 10;
    public float senseDelay = 1;
    private float lastSense;
    public string senseName = "";

    private bool isSensing = false;

    public int LayerID;

    protected int layerMask;

    protected SenseManager senseManager;

	void Start () {
        layerMask = 1 << LayerID;
        lastSense = Time.time - senseDelay;
        senseManager = GetComponent<SenseManager>();

	}

    void Update()
    {
        sense();
    }

    private void sense()
    {
        if (!isSensing && Time.time - lastSense >= senseDelay)
        {
            isSensing = true;
            senseNewObjects();
            //Debug.Log(gameObject.name + ": Sense from " + senseName);
            lastSense = Time.time;
            isSensing = false;
        }
    }

    protected abstract void senseNewObjects();

}
