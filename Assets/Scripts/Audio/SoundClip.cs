﻿using UnityEngine;
using System.Collections;

public class SoundClip 
{
    public AudioClip audioClip;
    public SoundClipTypeEnum soundClipType;

    public SoundClip(AudioClip _audioClip, SoundClipTypeEnum _soundClipType)
    {
        audioClip = _audioClip;
        soundClipType = _soundClipType;
    }
}