﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum SoundClipTypeEnum
{
    WoodChop1,
    WoodChop2,
    WoodChop3,
    Dig1,
    Dig2,
    BowPull,
    BowRelease,
    Whip,
    Splash,
    Reel,
    FishSplash,
    Thunder,
    Bite,
    BulletHitFlesh,
    RifleShot,
    DoorOpen,
    DoorKnock,
    DoorLock
}

public class SoundManager : MonoBehaviour
{

    List<SoundClip> soundClips;

    void Awake()
    {
        loadSounds();
    }

    public void play(SoundClipTypeEnum soundClipType)
    {
        playForDuration(soundClipType, null, 0, 0);
    }

    public void play(SoundClipTypeEnum soundClipType, Transform target)
    {
        playForDuration(soundClipType, target, 0, 0);
    }

    public void playAfterDelay(SoundClipTypeEnum soundClipType, Transform target, float delay)
    {
        playForDuration(soundClipType, target, 0, delay);
    }

    public void playForDuration(SoundClipTypeEnum soundClipType, Transform target, float duration, float delay)
    {
        if (target == null)
            target = transform;

        GameObject tempAudioGameObject = new GameObject();
        tempAudioGameObject.transform.position = target.position;

        AudioSource tempAudioSource = tempAudioGameObject.AddComponent<AudioSource>();
        float pitch = Random.Range(0.8f, 1f);
        Debug.Log(pitch);
        tempAudioSource.pitch = pitch;// 

        tempAudioSource.clip = getAudioClip(soundClipType);
        if (duration == 0)
            duration = tempAudioSource.clip.length * 2;

        StartCoroutine(startSound(tempAudioSource, duration, delay));
    }

    IEnumerator startSound(AudioSource tempAudioSource, float duration, float delay)
    {
        yield return new WaitForSeconds(delay);
        tempAudioSource.Play();
        StartCoroutine(stopSound(tempAudioSource, duration));

    }

    IEnumerator stopSound(AudioSource tempAudioSource, float duration)
    {
        yield return new WaitForSeconds(duration);
        //tempAudioSource.Stop();
        while (tempAudioSource.volume > 0.01)
        {
            tempAudioSource.volume -= 0.1f;
            yield return new WaitForSeconds(0.001f); // Wait roughly 1/10th frame? why not :)
        }

        Destroy(tempAudioSource.gameObject);
    }



    public void playAfterDelay(SoundClipTypeEnum soundClipType, AudioSource targetAudio, float delay)
    {

        StartCoroutine(playDelay(soundClipType, targetAudio, delay));

    }

    IEnumerator playDelay(SoundClipTypeEnum soundClipType, AudioSource targetAudio, float delay)
    {
        yield return new WaitForSeconds(delay);
        targetAudio.PlayOneShot(getAudioClip(soundClipType));
    }

    void loadSounds()
    {
        soundClips = new List<SoundClip>();
        soundClips.Add(new SoundClip(Resources.Load<AudioClip>("Sounds/whip"), SoundClipTypeEnum.Whip));
        soundClips.Add(new SoundClip(Resources.Load<AudioClip>("Sounds/reelmulti"), SoundClipTypeEnum.Reel));
        soundClips.Add(new SoundClip(Resources.Load<AudioClip>("Sounds/splash"), SoundClipTypeEnum.Splash));
        soundClips.Add(new SoundClip(Resources.Load<AudioClip>("Sounds/fishsplash"), SoundClipTypeEnum.FishSplash));
        soundClips.Add(new SoundClip(Resources.Load<AudioClip>("Sounds/thunder"), SoundClipTypeEnum.Thunder));
        soundClips.Add(new SoundClip(Resources.Load<AudioClip>("Sounds/bite"), SoundClipTypeEnum.Thunder));
        soundClips.Add(new SoundClip(Resources.Load<AudioClip>("Sounds/bowpull"), SoundClipTypeEnum.BowPull));
        soundClips.Add(new SoundClip(Resources.Load<AudioClip>("Sounds/bowrelease"), SoundClipTypeEnum.BowRelease));
        soundClips.Add(new SoundClip(Resources.Load<AudioClip>("Sounds/bullethit_flesh"), SoundClipTypeEnum.BulletHitFlesh));
        soundClips.Add(new SoundClip(Resources.Load<AudioClip>("Sounds/rifleshot"), SoundClipTypeEnum.RifleShot));
        soundClips.Add(new SoundClip(Resources.Load<AudioClip>("Sounds/dooropen"), SoundClipTypeEnum.DoorOpen));
        soundClips.Add(new SoundClip(Resources.Load<AudioClip>("Sounds/doorknock"), SoundClipTypeEnum.DoorKnock));
        soundClips.Add(new SoundClip(Resources.Load<AudioClip>("Sounds/doorlock"), SoundClipTypeEnum.DoorLock));
    }


    public AudioClip getAudioClip(SoundClipTypeEnum soundClipType)
    {
        for (int x = 0; x < soundClips.Count; x++)
        {
            if (soundClips[x].soundClipType == soundClipType)
            {
                //return soundClip.audioClip;
                return soundClips[x].audioClip;
            }
        }

        return null;
    }


}