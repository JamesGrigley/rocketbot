﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class Arrow : BaseBullet
{

    public override void addForce()
    {
        transform.SetParent(null);
        rigidbody.AddForce(transform.forward * 1000);
        //rigidbody.AddTorque(new Vector3(Random.Range(-maxSpinTorque, maxSpinTorque), Random.Range(-maxSpinTorque, maxSpinTorque), Random.Range(-maxSpinTorque, maxSpinTorque)));
        Destroy(gameObject, 4);
    }
}