﻿using UnityEngine;
using System.Collections;

public abstract class BaseBullet : MonoBehaviour {

    protected Rigidbody rigidbody;
    public GameObject shooter;
    public float maxSpinTorque = 100;
    public int damage = 10;
    public Transform explosion;
    Transform bloodSplatterPrefab;
    AudioSource audioSource;
    SoundManager soundManager;

    bool hit = false;

    void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
        soundManager = GameObject.Find("SoundManager").GetComponent<SoundManager>();
        bloodSplatterPrefab = Resources.Load<Transform>("Prefabs/BloodSplurtParticles");

    }

    void OnCollisionEnter(Collision col)
    {
        if (!hit)
        {
            hit = true;
            Debug.Log("I hit " + col.gameObject.name);

            soundManager.play(SoundClipTypeEnum.BulletHitFlesh, col.transform);

            if (explosion != null)
                Instantiate(explosion, transform.position, transform.rotation);

            BaseCharacter character = col.gameObject.GetComponent<BaseCharacter>();

            if (character != null)
            {
                Instantiate(bloodSplatterPrefab, transform.position, transform.rotation);
                character.takeDamage(damage, this);
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }

    public virtual void addForce()
    {
        rigidbody.AddForce(transform.forward * 300);
        rigidbody.AddTorque(new Vector3(Random.Range(-maxSpinTorque, maxSpinTorque), Random.Range(-maxSpinTorque, maxSpinTorque), Random.Range(-maxSpinTorque, maxSpinTorque)));
    }
}
