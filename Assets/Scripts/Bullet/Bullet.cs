﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class Bullet : BaseBullet
{

    void Start()
    {
        damage = 50;
        addForce();
    }

    public override void addForce()
    {
        transform.SetParent(null);
        rigidbody.AddForce(transform.forward * 5000);
        //rigidbody.AddTorque(new Vector3(Random.Range(-maxSpinTorque, maxSpinTorque), Random.Range(-maxSpinTorque, maxSpinTorque), Random.Range(-maxSpinTorque, maxSpinTorque)));
        Destroy(gameObject, 3);

    }
}