﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class CanExplosive : BaseBullet
{

    public override void addForce()
    {
        rigidbody.AddForce(new Vector3(Random.Range(300, 400), Random.Range(100, 200), 0));
        rigidbody.AddTorque(new Vector3(Random.Range(-maxSpinTorque, maxSpinTorque), Random.Range(-maxSpinTorque, maxSpinTorque), Random.Range(-maxSpinTorque, maxSpinTorque)));

    }
}