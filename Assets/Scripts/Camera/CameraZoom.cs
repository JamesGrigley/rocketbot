﻿using UnityEngine;
using System.Collections;

public class CameraZoom : MonoBehaviour {
    const int minZoom = -5;
    const int maxZoom = 5;

    float distPerZoom = 1f;

    float currentZoom = 0;

    private Vector3 currentVelocity;

    float originalZ;

    void Start()
    {
        originalZ = transform.position.z;
        //originalPosition = transform.position;
    }
	
	// Update is called once per frame
	void Update () {

        float scroll = Input.GetAxis("Mouse ScrollWheel");

        if (scroll > 0)
        {
            currentZoom += distPerZoom;
        }
        else if (scroll < 0)
        {
            currentZoom -= distPerZoom;
        }

        currentZoom = Mathf.Clamp(currentZoom, minZoom, maxZoom);

        Vector3 aheadTargetPos = new Vector3(transform.position.x, transform.position.y, originalZ + currentZoom);

        transform.position = Vector3.SmoothDamp(transform.position, aheadTargetPos, ref currentVelocity, 0.9f);
    }
}
