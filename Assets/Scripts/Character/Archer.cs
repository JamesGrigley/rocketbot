﻿using UnityEngine;
using System.Collections;

public class Archer : BaseNPC
{

    float holdSpeed;

    Arrow arrowPrefab;
    Arrow arrow;
    public Transform arrowSlot;

    protected override void Awake()
    {
        base.Awake();
        attackDistance = 30;
        arrowPrefab = Resources.Load<Arrow>("Prefabs/Arrow");
    }

    override public void attack()
    {
        transform.LookAt(attackingTarget.transform.position);
        animator.SetFloat("speed", 0);
        animator.SetTrigger("draw");
        soundManager.playAfterDelay(SoundClipTypeEnum.BowPull, transform, 0.5f);

        arrow = (Arrow)Instantiate(arrowPrefab, arrowSlot.position, arrowSlot.rotation);
        arrow.transform.SetParent(arrowSlot);
        //arrow.transform.localPosition = Vector3.zero;
        //arrow.transform.localRotation = new Quaternion();


        StartCoroutine(endAttack());
    }

    protected override void alert()
    {
        animator.SetBool("alert", true);
    }


    IEnumerator endAttack()
    {
        yield return new WaitForSeconds(2);
        animator.SetTrigger("shoot");
        soundManager.play(SoundClipTypeEnum.BowRelease, transform);

        arrow.transform.LookAt(attackingTarget.transform.position + Vector3.up * 1.5f);
        arrow.addForce();
        yield return new WaitForSeconds(2);
        isAttacking = false;
    }


    //public override void hit(float force)
    //{
    //    Debug.Log("Hit()");
    //    animator.SetTrigger("impact");
    //    holdSpeed = navMeshAgent.speed;
    //    navMeshAgent.speed = 0;
    //    animator.SetFloat("speed", 0);
    //    StartCoroutine(endHit());
    //}

    //IEnumerator endHit()
    //{
    //    yield return new WaitForSeconds(1);
    //    navMeshAgent.speed = holdSpeed;
    //    animator.SetFloat("speed", 1);

    //}

    //public override void die()
    //{
    //    animator.SetTrigger("die");
    //    navMeshAgent.Stop();
    //    GetComponent<BoxCollider>().enabled = false;
    //}




}