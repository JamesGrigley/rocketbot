﻿using UnityEngine;
using System.Collections;

public abstract class BaseCharacter : MonoBehaviour
{
    protected Health health;
    protected Animator animator;
    protected SoundManager soundManager;
    protected AudioSource audioSource;
    protected DialogManager dialogManager;

    bool isDying = false;

    protected virtual void Awake()
    {
        health = GetComponent<Health>();
        animator = GetComponent<Animator>();
        soundManager = GameObject.Find("SoundManager").GetComponent<SoundManager>();
        audioSource = GetComponent<AudioSource>();
        dialogManager = GameObject.Find("SceneKeeper").GetComponent<DialogManager>();
    }
    
    public virtual void Start() { }
    public virtual void die() { }
    public virtual void hit(float force) { }
    public virtual void attack() { }
    
    public virtual void takeDamage(int damage, BaseBullet bullet)
    {
        if (isDying)
            return;

        Debug.Log(gameObject.name + " says ouch for " + damage + " damage, shot by " + bullet.shooter.name);
        if (health.takeDamage(damage) == 0)
        {
            isDying = true;
            die();
        }
        else
        {
            hit(0.5f);
        }
        if (bullet != null)
         Destroy(bullet.gameObject);
    }
}