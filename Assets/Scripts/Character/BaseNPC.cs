﻿using UnityEngine;
using System.Collections;


public class BaseNPC : BaseCharacter
{
    protected NavMeshAgent navMeshAgent;
    protected GameObject attackingTarget = null;
    protected CombatNodes combatNodes;
    protected Vector3 destination;
    protected bool isAttacking = false;

    protected float attackDistance = 2;
    protected float standDistaince = 2;

    protected float walkSpeed = 1;

    bool isHostile = false;

    public delegate void GotoAndPerformDelegate();

    GotoAndPerformDelegate gotoAndPerformDelegate = null;

    protected override void Awake()
    {
        base.Awake();
        navMeshAgent = GetComponent<NavMeshAgent>();
        destination = navMeshAgent.destination;
    }


    void Update()
    {
        if (attackingTarget != null)
        {
            if (!isAttacking && Vector3.Distance(transform.position, attackingTarget.transform.position) <= attackDistance)
            {
                navMeshAgent.speed = 0;
                isAttacking = true;
                attack();

            }
            else if(Vector3.Distance(destination, combatNodes.getClosestNode(transform)) > attackDistance)
            {
                navMeshAgent.speed = walkSpeed;
                setCombatDestination();
            }
        }

        if (walkSpeed > 0 && Vector3.Distance(transform.position, destination) <= standDistaince)
        {
            navMeshAgent.speed = 0;
            animator.SetFloat("speed", 0);
            if (gotoAndPerformDelegate != null)
            {
                gotoAndPerformDelegate();
                gotoAndPerformDelegate = null;
            }
        }

    }

    protected virtual void alert() { }

    public void senseTarget(GameObject alertObject)
    {
        Debug.Log("Sensed target");
        alert();

        if (isHostile)
        {
            attackingTarget = alertObject;
            combatNodes = attackingTarget.GetComponent<CombatNodes>();
            setCombatDestination();
        }
    }

    void setCombatDestination()
    {
        destination = combatNodes.getClosestNode(transform);
        navMeshAgent.SetDestination(destination);
        animator.SetFloat("speed", walkSpeed);
    }

    public void setDesitination(Transform target)
    {
        destination = target.position;
        navMeshAgent.SetDestination(destination);
        animator.SetFloat("speed", walkSpeed);
    }

    public void gotoAndPerform(Transform target, GotoAndPerformDelegate _gotoAndPerformDelegate)
    {
        setDesitination(target);
        gotoAndPerformDelegate = _gotoAndPerformDelegate;
    }

}

