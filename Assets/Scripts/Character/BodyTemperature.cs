﻿using UnityEngine;
using System.Collections;

public class BodyTemperature : MonoBehaviour
{
    float baseBodytemperature = 70; // because I can't figure out why our body temp is 97.8 but we're hot when it's 97.8 F outside..
    public float currentBodytemperature;

    public float outsideTemperature = 70;

    public float hungerBurnRate = 1;


    // i need to burn hunger to keep warm

    Hunger hunger;
    bool isWet = false;

    void Awake()
    {
        hunger = GetComponent<Hunger>();
        currentBodytemperature = baseBodytemperature;
    }

    void Start()
    {
        StartCoroutine(runLoop());
    }

    IEnumerator runLoop()
    {
        while (true)
        {
            yield return new WaitForSeconds(5);
            adjustBodyTemp();
        }
    }

    void adjustBodyTemp()
    {
        // I need to adjust by
        // outside temp
        // wet?


        if (outsideTemperature < currentBodytemperature)
        {
            // I'm getting cold

            // burn hunger to increase current body temperate to base temperature

            currentBodytemperature -= (baseBodytemperature - outsideTemperature) * 0.1f; // because i'm getting colder

            if (hunger.burnHunger(hungerBurnRate))
            {
                currentBodytemperature += hungerBurnRate; // because i'll burn hunger
            }


        }
        else if (outsideTemperature > currentBodytemperature)
        {
            // I'm getting hot

            // Burn water to cool
        }

        // and then burn body resources accordingly
        burn();
    }

    void burn()
    {

        
    }


}