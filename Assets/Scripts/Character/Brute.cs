﻿using UnityEngine;
using System.Collections;

public class Brute : BaseNPC
{
    protected override void Awake()
    {
        base.Awake();
        walkSpeed = 2;
    }


    public override void hit(float force)
    {
        Debug.Log("Hit()");
        animator.SetTrigger("impact");

        navMeshAgent.speed = 0;
        animator.SetFloat("speed", 0);
        StartCoroutine(endHit());
    }

    IEnumerator endHit()
    {
        yield return new WaitForSeconds(1);
        navMeshAgent.speed = walkSpeed;
        animator.SetFloat("speed", 1);

    }

    public override void die()
    {
        animator.SetTrigger("die");
        navMeshAgent.Stop();
        GetComponent<BoxCollider>().enabled = false;
    }



    override public void attack()
    {
        animator.SetFloat("speed", 0);
        animator.SetTrigger("attack");
        transform.LookAt(attackingTarget.transform.position);
        StartCoroutine(endAttack());
    }


    IEnumerator endAttack()
    {
        yield return new WaitForSeconds(1);

        isAttacking = false;
    }
}