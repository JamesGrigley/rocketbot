﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Health : MonoBehaviour
{
    int maxHealth = 100;
    public int currentHealth;

    List<Wound> wounds;

    BaseCharacter baseCharacter;

    public void Awake()
    {
        wounds = new List<Wound>();
        baseCharacter = GetComponent<BaseCharacter>();
        currentHealth = maxHealth;
    }

    void Start()
    {
        StartCoroutine(test());
    }

    IEnumerator test()
    {
        yield return new WaitForSeconds(5);
        wounds.Add(new Wound(WoundTypeEnum.Arm));
    }
    public int takeDamage(int damage)
    {
        // need to create wounds here I guess


        currentHealth -= damage;

        if (currentHealth <= 0)
        {
            //Debug.Log("I should die");
            currentHealth = 0;

        }
        return currentHealth;
    }

    public bool heal()
    {
        if (wounds.Count == 0)
        {
            return false;
        }

        foreach (Wound wound in wounds)
        {
            if (wound.woundStatus == WoundStatusEnum.New)
            {
                wound.treat();
                return true;
            }
                 
        }
        return false;
    }

    public string statusText
    {
        get {

            updateWounds();

            if (wounds.Count == 0)
            {
                return "Healthy";
            }

            if (isWounded)
                return "Wounded";
            else if (isRecovering)
            {
                return "Recovering";
            }
            return "";

        }

    }

    public string futureText
    {
        get {
            if (wounds.Count == 0)
            {
                return "Healthy and ready to go!";
            }
            
            if (isWounded)
            {
                return "I am wounded. I should use a bandage.";
            }
            else if (isRecovering)
            {
                return "I have treated my wound. I should recover soon.";
            }
            return "";
        }

    }

    public bool isRecovering
    {
        get {
            foreach (Wound wound in wounds)
            {
                if (wound.woundStatus == WoundStatusEnum.Treated)
                {
                    return true;
                }

            }
            return false;
        }
    }

    public bool isWounded
    {
        get
        {
            foreach (Wound wound in wounds)
            {
                if (wound.woundStatus == WoundStatusEnum.New)
                {
                    return true;
                }

            }
            return false;
        }
    }

    void updateWounds()
    {

        List<Wound> woundsToRemove = new List<Wound>();

        foreach (Wound wound in wounds)
        {
            if (wound.isHealed())
            {
                woundsToRemove.Add(wound);
            }
        }

        foreach(Wound wound in woundsToRemove)
        {
            wounds.Remove(wound);
        }
    }

    public Color statusColor
    {
        get
        {
            if (isWounded)
            {
                return new Color(1, 0, 0, 0.1f);
            }
            else if (isRecovering)
            {
                return new Color(1, 1, 0, 0.1f);
            }
            else
            {
                return new Color(0, 1, 0, 0.1f);
            }
        }
    }

}

public enum WoundTypeEnum
{
    Arm
}

public enum WoundStatusEnum
{ 
    New,
    Treated
}

public class Wound
{
    WoundTypeEnum woundType;
    public WoundStatusEnum woundStatus = WoundStatusEnum.New;
    float woundTime;
    float healStartTime;
    float healDuration = 20;

    public Wound(WoundTypeEnum _woundType)
    {
        woundTime = Time.time;
        woundType = _woundType;
    }

    public void treat()
    {
        woundStatus = WoundStatusEnum.Treated;
        healStartTime = Time.time;
    }

    public bool isHealed()
    {
        if (woundStatus == WoundStatusEnum.Treated && healStartTime + healDuration <= Time.time)
        {
            return true;
        }
        return false;
    }
}
