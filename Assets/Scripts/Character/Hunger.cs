﻿using UnityEngine;
using System.Collections;

public class Hunger : MonoBehaviour
{
    float maxHunger = 100;
    public float currentHunger;

    BaseCharacter baseCharacter;

    float hungerPerSecond = 0.0f;
    float hungerDamageDelay = 2;
    float lastHungerDamage;

    SoundManager soundManager;

    void Awake()
    {
        currentHunger = maxHunger;
        baseCharacter = GetComponent<BaseCharacter>();
        lastHungerDamage = Time.time;
        soundManager = GameObject.Find("SoundManager").GetComponent<SoundManager>();

    }

    void Update()
    {
        currentHunger = Mathf.Clamp(currentHunger -= (hungerPerSecond * Time.deltaTime), 0, maxHunger);

        if (currentHunger == 0)
        {
            doHungerDamage();
        }

    }

    public bool eat(float hunger)
    {
        if(currentHunger == maxHunger)
        {
            return false;
        }
        else
        {
            currentHunger = Mathf.Clamp(currentHunger + hunger, 0, maxHunger);
            soundManager.play(SoundClipTypeEnum.Bite);
            return true;
        }
    }

    public bool burnHunger(float burnRate)
    {
        if (currentHunger >= burnRate)
        {
            currentHunger -= burnRate;
            return true;
        }
        else
        {
            return false;
        }
    }


    void doHungerDamage()
    {
        if (Time.time - lastHungerDamage > hungerDamageDelay)
        {
            lastHungerDamage = Time.time;
            baseCharacter.takeDamage(10, null);
        }

    }

    bool isWellFed
    {
        get { return currentHunger >= 90; }
    }

    bool isSatisfied
    {
        get { return currentHunger >= 70; }
    }

    public bool isHungry
    {
        get { return currentHunger < 70; }
    }

    public bool isStarving
    {
        get { return currentHunger < 20; }
    }

    public bool isDying
    {
        get { return currentHunger == 0; }
    }

    public string hungerStatusText
    {
        get {
            if (isWellFed)
            {
                return "Well-fed";
            }else if (isSatisfied)
            {
                return "Satisfied";
            } if (isDying)
            {
                return "Dying";
            }
            else if (isStarving)
            {
                return "Starving";
            }
            else if (isHungry)
            {
                return "Hungry";
            }

            return "";
        }
    }

    public string hungerFutureText
    {
        get
        {
            if (isSatisfied)
            {
                return "I've eaten well and can go several days without food if needed.";
            }
            else if (isStarving)
            {
                return "I'm starving, I should find food immediately.";
            }
            else if (isHungry)
            {
                return "I'm getting hungry, I should make sure I have food to eat today.";
            }
            return "";
        }
    }

    public Color statusColor
    {
        get
        {
            if (isSatisfied)
            {
                return new Color(0, 1, 0, 0.1f);
            }
            else if (isStarving)
            {
                return new Color(1, 0, 0, 0.1f);
            }
            else if (isHungry)
            {
                return new Color(1, 1, 0, 0.1f);
            }
            return Color.black;
        }
    }
}
