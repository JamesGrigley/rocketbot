﻿using UnityEngine;
using System.Collections;

public class Male : BaseNPC
{
    protected override void Awake()
    {
        base.Awake();
        walkSpeed = 1f;
    }

    public void knock()
    {
        animator.SetTrigger("knock");
        soundManager.play(SoundClipTypeEnum.DoorKnock);
        DialogManager.spawnSpeechBubble(transform, "Wazzzzupppp!?!?!?");
    }
}