﻿using UnityEngine;
using System.Collections;

public class Wellbeing : MonoBehaviour
{
    Health health;
    Hunger hunger;

    void Awake()
    {
        health = GetComponent<Health>();
        hunger = GetComponent<Hunger>();
    }

    public string statusText
    {
        get
        {
            if (health.isWounded || hunger.isStarving)
            {
                return "Sad";
            }
            else if (health.isRecovering || hunger.isHungry)
            {
                return "Content";
            }

            return "Happy";
        }
    }

    public string futureText
    {
        get
        {
            if (health.isWounded)
            {
                return "I'm wounded, it really hurts.";
            }
            else if (hunger.isStarving)
            {
                return "I'm starving, I wish I had something to eat.";
            }
            else if (health.isRecovering || hunger.isHungry)
            {
                return "I'm doing OK";
            }

            return "I'm doing great";
        }
    }

    public Color statusColor
    {
        get
        {
            if (statusText == "Sad")
            {
                return new Color(1, 0, 0, 0.1f);
            }
            else if (statusText == "Content")
            {
                return new Color(1, 1, 0, 0.1f);
            }
            else
            {
                return new Color(0, 1, 0, 0.1f);
            }
        }
    }

}