﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class RBLogo : MonoBehaviour
{
    Image logoImage;
    Transform ship;
    Camera camera;
    Canvas canvas;
    Vector2 xBounds;
    public GameObject particles;
    public GameObject particles2;

    void Awake()
    {
        logoImage = transform.FindChild("Image").GetComponent<Image>();
        ship = GameObject.Find("Ship").transform;
        camera = GameObject.Find("Camera").GetComponent<Camera>();
        canvas = GameObject.Find("Canvas").GetComponent<Canvas>();
        particles = (GameObject)Resources.Load("Prefabs/Smoke");
    }

    void Start()
    {
        xBounds = new Vector2(logoImage.rectTransform.position.x - logoImage.rectTransform.sizeDelta.x / 2, logoImage.rectTransform.position.x + logoImage.rectTransform.sizeDelta.x / 2);
    }


    float startLogoFillx = 350;
    float logoFillWidth = 1070;
    float logoShipBuffer = 900;
    float particleTime = 0.01f;
    float currentParticleTime;

    void FixedUpdate()
    {
        float shipX = WorldToCanvas(ship.transform.position).x;

        float fillAmount = percentOfRange(new Vector2(startLogoFillx - logoFillWidth, startLogoFillx), shipX + logoShipBuffer, false);
       // Debug.Log("fillAmount: " + fillAmount + "; ship: " + shipX);
        logoImage.fillAmount = fillAmount;

        if(currentParticleTime <= 0)
        {
            currentParticleTime = particleTime;

            if (shipX < 500 && shipX > -1900)
                createParticle(particles);

            if (shipX < 150 && shipX > -900)
                createParticle(particles2);

        }
        currentParticleTime -= Time.deltaTime;
    }

    void createParticle(GameObject particle)
    {
        GameObject instParticle = (GameObject)Instantiate(particle, ship.position + (Vector3.right * 8) + (-Vector3.up * 0.3f), ship.rotation);
        GameObject.Destroy(instParticle, 10);
    }

    Vector2 WorldToCanvas(Vector3 world_position)
    {
        if (camera == null)
        {
            camera = Camera.main;
        }

        var viewport_position = camera.WorldToViewportPoint(world_position);
        var canvas_rect = canvas.GetComponent<RectTransform>();

        return new Vector2((viewport_position.x * canvas_rect.sizeDelta.x) - (canvas_rect.sizeDelta.x * 0.5f),
                           (viewport_position.y * canvas_rect.sizeDelta.y) - (canvas_rect.sizeDelta.y * 0.5f));
    }

    float percentOfRange(Vector2 range, float value, bool doX)
    {
        if (doX)
            return Mathf.Clamp((value - range.x) / (range.y - range.x), 0.0f, 1.0f);
        else
            return Mathf.Clamp((value - range.y) / (range.x - range.y), 0.0f, 1.0f);
    }
}