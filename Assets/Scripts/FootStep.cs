﻿using UnityEngine;
using System.Collections;

public class FootStep : MonoBehaviour {
    public AudioClip[] footSoundsGrass;
    public AudioClip[] footSoundsWood;
    public AudioClip[] footSoundsSolid;
    AudioSource audioSource;

    public Vector2 pitchRange;

    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    void OnCollisionEnter(Collision collision)
    {
        Debug.Log("IsColliding with " + collision.transform.name);
    }

    void OnTriggerEnter(Collider col)
    {
        audioSource.pitch = Random.Range(pitchRange.x, pitchRange.y);

        if (col.gameObject.tag == "GrassTerrain")
        {
            audioSource.PlayOneShot(footSoundsGrass[Random.Range(0, footSoundsGrass.Length - 1)]);
        }
        else
        if (col.gameObject.tag == "WoodGround")
        {
            audioSource.PlayOneShot(footSoundsWood[Random.Range(0, footSoundsWood.Length - 1)]);
        }else
        if (col.gameObject.tag == "StoneTerrain")
        {
            audioSource.PlayOneShot(footSoundsSolid[Random.Range(0, footSoundsSolid.Length - 1)]);
        }
    }
}
