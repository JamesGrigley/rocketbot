﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameOver : MonoBehaviour
{
    Image gameOverBackground;
    Text gameOverText;

    bool gameEnding = false;

    void Awake()
    {
        gameOverBackground = GameObject.Find("GameOver").GetComponent<Image>();
        gameOverText = gameOverBackground.transform.FindChild("Text").GetComponent<Text>();
    }

    void Update()
    {
        if (gameEnding)
        {
            fadeIn();
        }
    }

    public bool endGame()
    {
        bool didEnd = !gameEnding;
        StartCoroutine(startFade());
        return didEnd;

    }

    IEnumerator startFade()
    {
        if (!gameEnding)
        {
            gameEnding = true;

            yield return new WaitForSeconds(3);
            fadeImage = true; 
            fadeStartTime = Time.time;
            yield return new WaitForSeconds(1.5f);
            fadeText = true;
            fadeTextStartTime = Time.time;
            yield return new WaitForSeconds(2);
            gameOverText.color = new Color(gameOverText.color.r, gameOverText.color.g, gameOverText.color.b, 0);
            int survivedDays = GameObject.Find("Sky").GetComponent<Sky>().currentDay - 1;
            gameOverText.text = getGameOverMessage(survivedDays);
            fadeTextStartTime = Time.time;
            yield return new WaitForSeconds(2);
            gameOverText.color = new Color(gameOverText.color.r, gameOverText.color.g, gameOverText.color.b, 0);
            gameOverText.text = getGameOverMessage2(survivedDays);
            fadeTextStartTime = Time.time;

        }
    }

    float fadeStartTime;
    float fadeTextStartTime;
    bool fadeImage = false;
    bool fadeText = false;

    void fadeIn()
    {
        if (fadeImage)
            gameOverBackground.color = new Color(gameOverBackground.color.r, gameOverBackground.color.g, gameOverBackground.color.b, Mathf.Lerp(0.0f, 1.0f, (Time.time - fadeStartTime) * 0.4f));
        if (fadeText)
            gameOverText.color = new Color(gameOverText.color.r, gameOverText.color.g, gameOverText.color.b, Mathf.Lerp(0.0f, 1.0f, (Time.time - fadeTextStartTime) * 0.5f));
    }

    string getGameOverMessage(int daysSurvived)
    {
        if (daysSurvived == 0)
            return "You didn't even survive the day...";
        else if (daysSurvived == 1)
            return "You barely survived a day...";
        else if (daysSurvived > 1)
            return "You survived " + daysSurvived + " days";
        else return "";

    }

    string getGameOverMessage2(int daysSurvived)
    {
        if (daysSurvived < 2)
            return "You suck...";
        else if (daysSurvived < 5)
            return "Not bad for a loser";
        else if (daysSurvived < 20)
            return "Pretty good!";
        else return "";

    }
}