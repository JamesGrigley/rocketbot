﻿using UnityEngine;
using System.Collections;


public class GardenPlant : BaseContainer
{
    float phaseStartTime;
    float phase1Duration = 5;
    float phase2Duration = 10;
    float phase3Duration = 10;
    float currentPhase = 0;
    float currentPhaseDuration = 0;

    Vector3 baseScale;
    Vector3 targetScale;

    void Awake()
    {
        baseScale = transform.localScale;

        base.Awake();

    }

    void Start()
    {
        base.Start();

        icon.toggleVisible(false);

        resetPlot();
    }

    void Update()
    {
        if (currentPhase > 0 && Time.time - currentPhaseDuration >= phaseStartTime)
            startNextPhase();

        if (currentPhase > 0)
            transform.localScale = Vector3.Lerp(transform.localScale, targetScale, Random.Range(0.01f, 0.3f) * Time.deltaTime);
    }

    void startNextPhase()
    {
        if (currentPhase <=4)
            currentPhase++;
        phaseStartTime = Time.time;

        Debug.Log("Entering phase " + currentPhase);
        if (currentPhase == 1)
        {
            gameObject.GetComponent<MeshRenderer>().enabled = true;
            icon.toggleVisible(false);

            targetScale = baseScale * Random.Range(2, 12);
            currentPhaseDuration = phase1Duration + Random.Range(-5, +5);
        }
        else if (currentPhase == 2)
        {
            targetScale = targetScale + Vector3.up * Random.Range(2, 12);
            currentPhaseDuration = phase2Duration + Random.Range(-5, +5);
        }
        else if (currentPhase == 3)
        {
            targetScale = targetScale + Vector3.right * Random.Range(0.1f, 0.3f);
            currentPhaseDuration = phase3Duration + Random.Range(-5, +5);
        }
        else if (currentPhase > 3)
        {
            icon.toggleVisible(true);
            icon.setIcon(IconTypeEnum.Inspect, 2, this.transform, clickable, beginSearch, endSearch);
        }

    }

    public void setPlot()
    {
        icon.toggleVisible(true);
    }

    void resetPlot()
    {
        currentPhase = 0;
        transform.localScale = baseScale;
        icon.setIcon(IconTypeEnum.Broken, activationTime, this.transform, clickable, beginSearch, endSearch);

        gameObject.GetComponent<MeshRenderer>().enabled = false;

    }

    public override void openContainer()
    {
        if (currentPhase == 0)
        {
            startNextPhase();
        }
        else
        {
            Debug.Log("I harvest the plant");
            inventory.addItem(ItemTypeEnum.Carrot, 1);

            resetPlot();

        }


    }

    public bool clickable()
    {
        return true;
    }

}
