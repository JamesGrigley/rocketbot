﻿using UnityEngine;
using System.Collections;


public class GardenPlot : BaseContainer
{
    GardenPlant plant;

    float digTime = 5;

    protected void Start()
    {
        plant = transform.GetChild(0).GetComponent<GardenPlant>();
     
        base.Start();
    }


    public override void openContainer()
    {
        icon.toggleVisible(false);
        plant.setPlot();

    }

    public override bool hasCorrectTool()
    {
        bool hasTool = inventory.getItem(ItemTypeEnum.Shovel).quantity > 0;

        return hasTool;
    }


}
