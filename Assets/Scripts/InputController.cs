﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InputController : MonoBehaviour {

    Player player;

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }

	// Update is called once per frame
	void Update () {
	    if(Input.GetKeyDown(KeyCode.Space))
        {
            activateLocal();
        }
	}

    void activateLocal()
    {
        BaseContainer[] baseContainers = GameObject.FindObjectsOfType<BaseContainer>();

        BaseContainer closestBaseContainer = null;
        foreach (BaseContainer baseContainer in baseContainers)
        {
            if (closestBaseContainer == null || Vector3.Distance(baseContainer.transform.position, player.transform.position) < Vector3.Distance(closestBaseContainer.transform.position, player.transform.position))
            {
                closestBaseContainer = baseContainer;
            }

        }

        if (closestBaseContainer != null)
        {
            player.goTo(closestBaseContainer.transform.position, false, closestBaseContainer.endSearch);
        }
    }
}
