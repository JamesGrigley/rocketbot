﻿using UnityEngine;
using System.Collections.Generic;

public enum ItemTypeEnum
{
    Trash,
    Rock,
    Wood,
    Metal,
    Axe,
    Shovel,
    Pickaxe,
    Hacksaw,
    Carrot,
    FishRaw,
    FishCooked,
    CarrotCooked,
    Bandage
}


public class Inventory : MonoBehaviour {

    public delegate void InventoryUpdatedDelegate(InventoryItem item);
    public InventoryUpdatedDelegate inventoryUpdatedDelegate;

    public List<InventoryItem> items;

    InventoryIndicatorManager inventoryIndicatorManager;

    void Awake()
    {
        items = new List<InventoryItem>();
        inventoryIndicatorManager = GameObject.Find("InventoryIndicatorManager").GetComponent<InventoryIndicatorManager>();
    }


    void updateIcons()
    {
        Icon[] icons = Transform.FindObjectsOfType<Icon>();
        foreach (Icon icon in icons)
        {
            icon.refresh();
        }
    }

    public InventoryItem getItem(ItemTypeEnum _itemType)
    {
        foreach (InventoryItem item in items)
        {
            if (item.itemType == _itemType)
            {
                return item;
            }
        }
        return new InventoryItem(_itemType, 0);
    }

    public List<InventoryItem> getResources()
    {
        List<InventoryItem> resourceItems = new List<InventoryItem>();

        foreach (InventoryItem item in items)
        {
            if (!item.isTool)
            {
                resourceItems.Add(item);
            }
        }
        return resourceItems;
    }

    public bool hasItems(List<InventoryItem> itemsToCheck)
    {
        foreach (InventoryItem item in itemsToCheck)
        {
            if(!(getItem(item.itemType).quantity >= item.quantity))
            {
                return false;
            }
        }

        return true;
    }

    public bool consumeItems(List<InventoryItem> itemsToConsume)
    {
        foreach (InventoryItem item in itemsToConsume)
        {
            getItem(item.itemType).quantity -= item.quantity;
        }

        return true;
    }

    public void addItem(ItemTypeEnum _itemType, int _quantity)
    {
        if (_quantity == 0) return;

        InventoryItem newItem = null;

        if(_quantity > 0)
            inventoryIndicatorManager.showIndicator(_itemType, _quantity);

        foreach (InventoryItem item in items)
        {
            if (item.itemType == _itemType)
            {
                item.quantity += _quantity;
                newItem = item;
                break;
            }
        }

        // didn't find a match, add new item
        if (newItem == null)
        {
            newItem = new InventoryItem(_itemType, _quantity);
            items.Add(newItem);

        }

        if (inventoryUpdatedDelegate != null)
            inventoryUpdatedDelegate(newItem);

        updateIcons();
    }
}
