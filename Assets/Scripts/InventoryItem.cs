﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InventoryItem 
{
    ItemTypeEnum _itemType;
    public ItemTypeEnum itemType
    {
        get { return _itemType; }
        set
        {
            if (_itemType != value)
            {
                _itemType = value;
                //if (inventoryIcon != null)
                //    refresh();
            }
        }
    }
    public int quantity;
    public bool isTool
    {
        get
        {
            if (itemType == ItemTypeEnum.Axe || itemType == ItemTypeEnum.Pickaxe || itemType == ItemTypeEnum.Hacksaw)
                return true;
            else
                return false;
        }
    }

    public bool isCookable
    {
        get
        {
            if (itemType == ItemTypeEnum.FishRaw || itemType == ItemTypeEnum.Carrot)
                return true;
            else
                return false;
        }
    }

    public bool isBurnable
    {
        get
        {
            if (itemType == ItemTypeEnum.Trash || itemType == ItemTypeEnum.Wood)
                return true;
            else
                return false;
        }
    }

    public InventoryItem() { }

    public InventoryItem(ItemTypeEnum _itemType, int _quantity)
    {
        itemType = _itemType;
        quantity = _quantity;
    }

    public Sprite iconSprite
    {
        get { return InventoryUIItem.getIcon(itemType); }
    } 
}