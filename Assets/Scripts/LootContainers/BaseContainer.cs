﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public abstract class BaseContainer : MonoBehaviour {

    public List<AudioClip> audioClips;

    protected AudioSource audioSource;

    public delegate void AfterSearchDelegate();

    protected Inventory inventory;
    Player player;
    protected PlayerStats playerStats;

    public Icon icon;
    public IconTypeEnum iconType;
    public float lootPosition;


    public float activationTime;

    // Use this for initialization
    public void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    protected void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();

        inventory = player.gameObject.GetComponent<Inventory>();
        playerStats = player.gameObject.GetComponent<PlayerStats>();

        icon = IconSpawner.spawnIcon();
        icon.setIcon(iconType, activationTime, this.transform, hasCorrectTool, beginSearch, endSearch);        
    }

    public void beginSearch()
    {
        player.beginSearch(lootPosition, activationTime);
    }

    public void endSearch()
    {
        playSounds();
        openContainer();
        player.endSearch();

    }

    virtual public void openContainer() { }
    virtual public bool hasCorrectTool() { return true; }

    void playSounds()
    {
        float waitTime = 0;

        foreach (AudioClip clip in audioClips)
        {
            StartCoroutine(playNextSound(clip, waitTime));
            waitTime += clip.length;
        }
    }

    IEnumerator playNextSound(AudioClip clip, float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        audioSource.clip = clip;
        audioSource.Play();
    }
}
