﻿using UnityEngine;
using System.Collections;

public class BaseRepairContainer : BaseContainer
{
    RepairWindow repairWindow;

    public float repairTime;

    public IconTypeEnum repairingIcon;


    // Use this for initialization
    void Start () {
        repairWindow = GetComponent<RepairWindow>();
        repairWindow.startRepairDelegate = startRepair;
        base.Start();
    }

    // Update is called once per frame
    void Update () {
	
	}

    public override void openContainer()
    {
        repairWindow.activate();
    }

    public void startRepair()
    {
        icon.iconType = repairingIcon;
        icon.activationTime = repairTime;
        //icon.activateCallBackDelegate = repairFinished;
        icon.activate();
    }

    virtual public void repairFinished(AfterSearchDelegate callback = null)
    {

    }

}
