﻿using UnityEngine;
using System.Collections;

public class Campfire : BaseContainer
{

    Transform fire;
    CampfireWindow campfireWindow;

    bool fireOn = false;

    void Awake()
    {
        iconType = IconTypeEnum.Campfire;
        fire = transform.FindChild("Fire");
        fire.gameObject.SetActive(false);
        campfireWindow = GameObject.Find("CampfireWindow").GetComponent<CampfireWindow>();
        base.Awake();
    }

    public override void openContainer()
    {
        campfireWindow.toggleWindow(this);
        //toggleFire();
    }

    public void toggleFire(bool _fireOn)
    {
        fireOn = !_fireOn; // flip because it flips back in next toggle
        toggleFire();
    }

    void toggleFire()
    {
        fireOn = !fireOn;
        fire.gameObject.SetActive(fireOn);
        //StartCoroutine(delayFireOn());
    }

    //IEnumerator delayFireOn()
    //{
    //    yield return new WaitForSeconds(activationTime);
        

    //}

}