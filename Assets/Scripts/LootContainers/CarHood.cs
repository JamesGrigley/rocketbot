﻿using UnityEngine;
using System.Collections;

public class CarHood : BaseContainer {

    Camera camera;
    Vector3 currentVelocity;

    Vector3 originalPosition;
    Quaternion originalRotation;

    Vector3 targetPosition;
    Quaternion targetRotation;


    float cameraPanLength = 1;

    bool looting = false;
    float lootStartTime;

    void Awake()
    {
        iconType = IconTypeEnum.Inspect;
        activationTime = 1;
        camera = GameObject.Find("Camera").GetComponent<Camera>();
        base.Awake();
    }

    float i = 0.0f;


    void Update()
    {
        if (looting)
        {
            camera.transform.position = Vector3.SmoothDamp(camera.transform.position, targetPosition, ref currentVelocity, cameraPanLength);

            //camera.transform.rotation = Quaternion.Lerp(originalRotation, targetRotation, (Time.time  - lootStartTime) * 3);

            while (i < 1.0)
            {
                i += Time.deltaTime * 0.01f;
                camera.transform.rotation = Quaternion.Lerp(originalRotation, targetRotation, i);
            }


        }
    }

    public override void openContainer()
    {
        camera.GetComponent<SmoothFollow2D>().enabled = false;
        camera.GetComponent<CameraZoom>().enabled = false;

        originalPosition = camera.transform.position;
        originalRotation = camera.transform.rotation;

        targetPosition = transform.FindChild("CameraTarget").position;
        targetRotation = transform.FindChild("CameraTarget").rotation;

        lootStartTime = Time.time;

        looting = true;

    }
}
