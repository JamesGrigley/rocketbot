﻿using UnityEngine;
using System.Collections;

public class Charger : BaseContainer
{

    void Awake()
    {
        iconType = IconTypeEnum.ElectricCharge;
        //activationTime = 1;
        base.Awake();
    }

    public override void openContainer()
    {
        //playerStats.currentEnergy = playerStats.maxEnergy;
        Destroy(this.gameObject);
    }
}
