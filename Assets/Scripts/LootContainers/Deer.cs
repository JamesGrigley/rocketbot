﻿using UnityEngine;
using System.Collections;

public class Deer : BaseContainer
{
    Camera camera;

    void Awake()
    {
        iconType = IconTypeEnum.Inspect;
        activationTime = 1;
        camera = GameObject.Find("Camera").GetComponent<Camera>();
        base.Awake();
    }
}
