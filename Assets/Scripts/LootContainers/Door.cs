﻿using UnityEngine;
using System.Collections;

public class Door : BaseContainer
{
    Animator animator;
    SoundManager soundManager;

    bool isOpen = false;

    void Awake()
    {
        base.Awake();
        animator = transform.parent.GetComponent<Animator>();
        soundManager = GameObject.Find("SoundManager").GetComponent<SoundManager>();
    }

    public override void openContainer()
    {
        toggleDoor(!isOpen);
    }

    void toggleDoor(bool _isOpen)
    {
        if(_isOpen && !isOpen)
        {
            isOpen = true;
            animator.SetTrigger("open");
            soundManager.play(SoundClipTypeEnum.DoorOpen);
        }else if(!_isOpen && isOpen)
        {
            isOpen = false;
            animator.SetTrigger("close");
            soundManager.playAfterDelay(SoundClipTypeEnum.DoorLock, audioSource, 0.85f);
        }
    }
}

