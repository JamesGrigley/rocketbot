﻿using UnityEngine;
using System.Collections;

public class FishingHole : BaseContainer {

    void Awake()
    {
        iconType = IconTypeEnum.Fishing;

        base.Awake();
    }

    public override void openContainer()
    {
        inventory.addItem(ItemTypeEnum.FishRaw, Random.Range(0, 4));
        inventory.addItem(ItemTypeEnum.Trash, Random.Range(0, 5));
        inventory.addItem(ItemTypeEnum.Wood, Random.Range(0, 5));

    }

    public override bool hasCorrectTool()
    {
        bool hasTool = true;//  inventory.getItem(ItemTypeEnum.Axe).quantity > 0;

        return hasTool;
    }

}
