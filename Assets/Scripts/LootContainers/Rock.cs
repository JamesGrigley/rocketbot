﻿using UnityEngine;
using System.Collections;

public class Rock : BaseContainer {


    public override void openContainer()
    {
        inventory.addItem(ItemTypeEnum.Rock, 1);
        Debug.Log("Now I has the rock!");
        Destroy(this.gameObject);
    }

    public override bool hasCorrectTool()
    {
        bool hasTool = inventory.getItem(ItemTypeEnum.Pickaxe).quantity > 0;

        //Debug.Log("I don't have a pickaxe!");

        return hasTool;
    }

}
