﻿using UnityEngine;
using System.Collections;

public class RoofRepair : BaseRepairContainer
{
    public GameObject theRoof;

    public override void repairFinished(AfterSearchDelegate callback = null)
    {
        theRoof.SetActive(true);
        GameObject.Destroy(this.gameObject);
    }


}


