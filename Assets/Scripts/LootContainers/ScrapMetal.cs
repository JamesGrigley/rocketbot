﻿using UnityEngine;
using System.Collections;

public class ScrapMetal : BaseContainer {


    public override void openContainer()
    {
        inventory.addItem(ItemTypeEnum.Metal, 1);
        Debug.Log("Now I has the scrap metals!");
        base.openContainer();
    }

    public override bool hasCorrectTool()
    {
        bool hasTool = inventory.getItem(ItemTypeEnum.Hacksaw).quantity > 0;
       
        return hasTool;
    }

}
