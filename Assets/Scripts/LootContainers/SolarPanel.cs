﻿using UnityEngine;
using System.Collections;

public class SolarPanel : BaseContainer
{

    RepairWindow repairWindow;

    void Awake()
    {
        repairWindow = GetComponent<RepairWindow>();

        if (iconType.ToString() == "")
            iconType = IconTypeEnum.ElectricCharge;
        //activationTime = 1;
        base.Awake();
    }

    public override void openContainer()
    {
        repairWindow.activate();
    }

}
