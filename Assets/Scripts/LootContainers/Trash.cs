﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Trash : BaseContainer
{
    public override void openContainer()
    {
        inventory.addItem(ItemTypeEnum.FishRaw, Random.Range(1, 4));
        inventory.addItem(ItemTypeEnum.Trash, Random.Range(5, 5));
        inventory.addItem(ItemTypeEnum.Wood, Random.Range(5, 5));
        //inventory.addItem(ItemTypeEnum.Rock, Random.Range(2, 5));
        inventory.addItem(ItemTypeEnum.Carrot, Random.Range(2, 5));
        Destroy(this.gameObject);
    }

}