﻿using UnityEngine;
using System.Collections;

public class Tree : BaseContainer {

    void Awake()
    {
        iconType = IconTypeEnum.Wood;

        base.Awake();
    }

    public override void openContainer()
    {
        inventory.addItem(ItemTypeEnum.Wood, 1);
        Debug.Log("Now I have wood!");
        Destroy(this.gameObject);
    }

    public override bool hasCorrectTool()
    {
        bool hasTool =  inventory.getItem(ItemTypeEnum.Axe).quantity > 0;

        return hasTool;
    }

}
