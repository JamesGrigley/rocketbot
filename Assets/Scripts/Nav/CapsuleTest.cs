﻿using UnityEngine;
using System.Collections;

public class CapsuleTest : MonoBehaviour {

    Animator animator;
    NavMeshAgent navMeshAgent;
    GameObject attackingTarget = null;
    CombatNodes combatNodes;

    Vector3 destination;

    bool isAttacking = false;

    void Start () {
        animator = GetComponent<Animator>();

        navMeshAgent = GetComponent<NavMeshAgent>();
        destination = navMeshAgent.destination;
        
        //navMeshAgent.SetDestination(GameObject.Find("Target").transform.position);
        //navMeshAgent.SetDestination(GameObject.FindGameObjectWithTag("Player").GetComponent<CombatNodes>().getClosestNode(transform.position));
        //navMeshAgent.updateRotation = false;
        //StartCoroutine(test());
    }
	
	void Update ()
    {
        if (attackingTarget != null)
        {
            if (!isAttacking && Vector3.Distance(transform.position, attackingTarget.transform.position) < 2f)
            {
                isAttacking = true;
                attack();

            }
            else
            {
                if (Vector3.Distance(destination, combatNodes.getClosestNode(transform)) > 1f)
                {
                    setDestination();
                }
            }
        }
    }

    public void senseTarget(GameObject alertObject)
    {
        Debug.Log("Sensed target");
        attackingTarget = alertObject;
        combatNodes = attackingTarget.GetComponent<CombatNodes>();
        setDestination();
        //StartCoroutine(doSense());
    }

    void setDestination()
    {
        destination = combatNodes.getClosestNode(transform);
        navMeshAgent.SetDestination(destination);

        animator.SetFloat("speed", 1);

    }

    void attack()
    {
        animator.SetFloat("speed", 0);
        animator.SetTrigger("attack");
        transform.LookAt(attackingTarget.transform.position);
        StartCoroutine(endAttack());
    }


    IEnumerator endAttack()
    {
        yield return new WaitForSeconds(1);

        isAttacking = false;
    }
}
