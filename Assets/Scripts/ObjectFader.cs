﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ObjectFader : MonoBehaviour
{
    Graphic[] fadingObjects;
    float fadeSpeed = 0.5f;

    bool isFading = false;

    void Awake()
    {
        fadingObjects = transform.GetComponentsInChildren<Graphic>();
    }

    void Start()
    {
        StartCoroutine(startFading());
    }

    void Update()
    {
        if (isFading)
        {
            foreach (Graphic image in fadingObjects)
            {
                if (image != null)
                {
                    image.CrossFadeAlpha(0, fadeSpeed, false);
                }
            }
        }
    }

    IEnumerator startFading()
    {
        yield return new WaitForSeconds(3);
        isFading = true;
        yield return new WaitForSeconds(3);
        GameObject.Destroy(this.gameObject);

    }

}
