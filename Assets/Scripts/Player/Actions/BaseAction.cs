﻿using UnityEngine;
using System.Collections;

public abstract class BaseAction : MonoBehaviour
{

    protected Animator animator;
    protected PlayerTools playerTools;
    protected SoundManager soundManager;

    protected CharacterController3D characterController;

    protected virtual void Awake()
    {
        animator = GetComponent<Animator>();
    }

    protected virtual void Start()
    {
        playerTools = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerTools>();
        soundManager = GameObject.Find("SoundManager").GetComponent<SoundManager>();
        characterController = GameObject.FindGameObjectWithTag("Player").GetComponent<CharacterController3D>();
    }

    public abstract bool execute();

}