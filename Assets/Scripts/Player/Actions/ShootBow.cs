﻿using UnityEngine;
using System.Collections;
using System;

public class ShootBow : BaseAction
{

    bool isShootingBow = false;

    public override bool execute()
    {
        if (!isShootingBow)
        {
            StartCoroutine(doShootBow());
            return true;
        }
        else
        {
            return false;
        }
    }

    IEnumerator doShootBow()
    {
        isShootingBow = true;
        playerTools.setTool(4);
        animator.SetTrigger("shootBow");
        yield return new WaitForSeconds(1.40f);
        soundManager.play(SoundClipTypeEnum.BowPull);
        characterController.flipY(90);
        playerTools.arrow.gameObject.SetActive(true);
        yield return new WaitForSeconds(1);
        
        playerTools.arrow.GetComponent<Arrow>().addForce();
        soundManager.play(SoundClipTypeEnum.BowRelease);
        yield return new WaitForSeconds(1);
        //characterController.flipY(-90);
        isShootingBow = false;
        playerTools.unsetTool();

    }


}