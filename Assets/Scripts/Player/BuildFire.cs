﻿using UnityEngine;
using System.Collections;

public class BuildFire : MonoBehaviour
{
    Transform campfirePrefab;
    Transform campfire;
    Camera camera;

    bool isBuildingFire = false;

    void Awake()
    {
        campfirePrefab = (Transform)Resources.Load<Transform>("Prefabs/Campfire");
        camera = GameObject.Find("Camera").GetComponent<Camera>();
    }

    void Update()
    {
        if (isBuildingFire)
        {
            RaycastHit hit;
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);
            int LayerID = 1 << 8;


            if (Physics.Raycast(ray, out hit, Mathf.Infinity, LayerID))
            {

                //Debug.Log(hit.point);
                Vector3 placement = new Vector3(hit.point.x, getSnapY(campfire.position), hit.point.z);
                campfire.position = placement;
            }

            if (Input.GetMouseButtonDown(0))
            {
                isBuildingFire = false;
            }
        }
    }

    float getSnapY(Vector3 startPosition)
    {
        float snapY = 0;
        RaycastHit hit;
        int LayerID = 1 << 8;

        if (Physics.Raycast(startPosition, Vector3.down, out hit, Mathf.Infinity, LayerID))
        {
            snapY = hit.point.y;
        }

        return snapY;
    }

    public void startFireBuilding()
    {
        campfire = (Transform)Instantiate(campfirePrefab);
        isBuildingFire = true;
    }

    
}

