﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Character2D))]
public class CharacterController2D : MonoBehaviour
{
    private Character2D m_Character;
    private bool m_Jump;
    private bool isCrouching = false;

    public bool isWalking
    {
        get { return walkStatus != 0; }
    }
    int walkStatus = 0;
    float gotoPosition = 0;

    float fullWalkspeed = 0.5f;
    float walkSpeed;

    PlayerPower playerPower;

    private GoToCallBackDelegate gotoPositionCallback;

    private void Awake()
    {
        m_Character = GetComponent<Character2D>();
        playerPower = GetComponent<PlayerPower>();
    }


    private void Update()
    {
        if (!m_Jump)
        {
            // Read the jump input in Update so button presses aren't missed.
            //m_Jump = CrossPlatformInputManager.GetButtonDown("Jump");
        }
    }


    private void FixedUpdate()
    {
        if (!playerPower.isPowered) // stop
        {
            walkSpeed = 0;
            walkStatus = 0;
        }
        else if (playerPower.energyPercent > 0.8f)
        {
            walkSpeed = fullWalkspeed;
        }
        else if (playerPower.energyPercent > 0.5f)
        {
            walkSpeed = fullWalkspeed * .8f;
        }
        else if (playerPower.energyPercent < .1)
        {
            walkSpeed = fullWalkspeed / 2;
        }

        // Read the inputs.
        //bool crouch = Input.GetKey(KeyCode.LeftControl);
        //float h = CrossPlatformInputManager.GetAxis("Horizontal");
        if (walkStatus != 0 && Mathf.Abs(transform.position.x - gotoPosition) < 0.1f)
        {
            walkStatus = 0;
            if (gotoPositionCallback != null)
            {
                gotoPositionCallback();
            }
        }

        // Pass all parameters to the character control script.
        m_Character.Move(walkStatus * walkSpeed, isCrouching, m_Jump);
        m_Jump = false;
    }

    public delegate void GoToCallBackDelegate();
    public void goTo(float _positionX, GoToCallBackDelegate callback)
    {
        if (!isCrouching)
        {
            walkStatus = (_positionX >= transform.position.x) ? 1 : -1;
            gotoPosition = _positionX;
            gotoPositionCallback = callback;
        }
    }

    public void crouch(bool _isCrouching)
    {
        isCrouching = _isCrouching;
    }
}