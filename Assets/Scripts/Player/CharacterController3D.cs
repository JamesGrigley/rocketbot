﻿using UnityEngine;
using System.Collections;

public delegate void goToCallbackDelegate();

public class CharacterController3D : MonoBehaviour
{

    CharacterController characterConroller;
    goToCallbackDelegate goToCallback;
    PlayerActions playerActions;

    public int directionFacing = 1;


    public bool isWalking
    {
        get { return walkStatus != 0; }
    }

    int walkStatus = 0;
    Vector3 gotoPosition;

    float walkSpeed = 0.8f;
    float runSpeed = 1.0f;
    float currentMoveSpeed;


    void Awake()
    {
        playerActions  = GetComponent<PlayerActions>();
        characterConroller = GetComponent<CharacterController>();
    }

    void Start()
    {
        playerActions.Stay();
    }

    void FixedUpdate()
    {
        if (walkStatus != 0 && Vector3.Distance(transform.position, gotoPosition) < 1.1f)
        {
            walkStatus = 0;
            if (goToCallback != null)
            {
                transform.LookAt(gotoPosition);
                goToCallback();
            }
        }

        // i want scale of 0-.9 is walking, >=1 running, so I'm going to tweak this
        float speedSyncFactor = 4;
        characterConroller.SimpleMove(Vector3.right * walkStatus * currentMoveSpeed * speedSyncFactor);

        if (isWalking)
            playerActions.Walk(currentMoveSpeed);
        else
            playerActions.Stay();
    }

    public void goTo(Vector3 _position, bool run, goToCallbackDelegate _goToCallback)
    {
        //walkStatus = (_positionX >= transform.position.x) ? 1 : -1;

        currentMoveSpeed = run ? runSpeed : walkSpeed;

        flipX();

        gotoPosition = _position;
        goToCallback = _goToCallback;
    }

    void flipX()
    {
        if (directionFacing != walkStatus)
        {
            //transform.Rotate(new Vector3(0, 180, 0));
        }
        directionFacing = walkStatus;
    }

    int isFlipped = -1;
    int directionAtFlip;
    float theRotation = 0;

    public void flipY(float rotation = 70)
    {
        // if this is the starting turn, grab the starting direction facing.
        if (isFlipped == -1)
        {
            directionAtFlip = directionFacing;
        }

        theRotation = rotation * isFlipped * directionAtFlip;
        doFlip = true;


    }

    bool doFlip = false;
    void LateUpdate()
    {
        if (doFlip)
        {
            doFlip = false;
            //transform.Rotate(new Vector3(0, theRotation, 0));

            isFlipped *= -1;

        }

    }
}