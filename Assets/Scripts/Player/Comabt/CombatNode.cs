﻿using UnityEngine;
using System.Collections;

public class CombatNode : MonoBehaviour
{

    public Transform attacker;
    public bool isFront = false;

    public Vector3 position
    {
        get
        {
            return transform.position;
        }
    }
}
