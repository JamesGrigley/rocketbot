﻿using UnityEngine;
using System.Collections;

public class Lazer : MonoBehaviour {

    Vector2 mouse;
    RaycastHit hit;
    float range = 100.0f;
    LineRenderer line;
    public Material lineMaterial;
    int LayerID = 1 << 8;

    bool canShoot = true;

    void Start()
    {
        line = GetComponent<LineRenderer>();
        line.SetVertexCount(2);
        line.GetComponent<Renderer>().material = lineMaterial;
        line.SetWidth(0.1f, 0.1f);
    }

    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, range, LayerID))
        {
            if (canShoot && Input.GetMouseButton(1))
            {
                canShoot = false;
                line.enabled = true;
                line.SetPosition(0, transform.parent.position);
                line.SetPosition(1, hit.point);
                StartCoroutine(stopLazer());
            }
        }

    }

    IEnumerator stopLazer()
    {
        yield return new WaitForSeconds(0.5f);
        line.enabled = false;
        canShoot = true;
    }

}
