﻿using UnityEngine;
using System.Collections;

public class Lazer2 : MonoBehaviour {

    Vector2 mouse;
    RaycastHit hit;
    float range = 100.0f;

    public Transform bullet;

    int LayerID = 1 << 8;

    bool canShoot = true;

    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, range, LayerID))
        {
            if (canShoot && Input.GetMouseButton(1))
            {
                canShoot = false;
                Transform bulletShot = (Transform)Instantiate(bullet, transform.position, transform.rotation);
                bulletShot.LookAt(new Vector3(hit.point.x, hit.point.y, transform.position.z));
                StartCoroutine(stopLazer());
            }
        }

    }

    IEnumerator stopLazer()
    {
        yield return new WaitForSeconds(1f);

        canShoot = true;
    }

}
