﻿using UnityEngine;
using System.Collections;

public class Player : BaseCharacter {

    Transform targetObject;
    CharacterController3D characterController;
    PlayerActions playerActions;
    PlayerTools playerTools;
    Hunger hunger;

    SceneKeeper sceneKeeper;

    public Inventory inventory;

    public bool isWalking;
    public bool isSearching = false;

    // Use this for initialization
    void Awake() {
        base.Awake();
        playerActions = GetComponent<PlayerActions>();
        playerTools = GetComponent<PlayerTools>();
        characterController = GetComponent<CharacterController3D>();
        inventory = GetComponent<Inventory>();
        hunger = GetComponent<Hunger>();
    }

    void Start() {
        base.Start();
        sceneKeeper = GameObject.Find("SceneKeeper").GetComponent<SceneKeeper>();
    }


    void Update()
    {
        isWalking = characterController.isWalking;
    }

    public override void takeDamage(int damage, BaseBullet bullet)
    {
        base.takeDamage(damage, bullet);
        playerActions.Hit(0.5f);
    }

    public void goTo(Vector3 position, bool run,goToCallbackDelegate callback = null)
    {
        if (!isSearching)
            characterController.goTo(position, run, callback);
    }

    public void beginSearch(float lootPosition, float duration)
    {
        //Debug.Log("Player.Loot " + lootPosition);
        isSearching = true;
        characterController.flipY();
        playerActions.beginLoot(lootPosition, characterController.directionFacing, duration);
        playerTools.setTool(lootPosition);
    }

    public void endSearch()
    {
        isSearching = false;

        // Debug.Log("Loot done");
        StartCoroutine(waitToFlip());
        playerTools.unsetTool();

    }

    IEnumerator waitToFlip()
    {
        yield return new WaitForSeconds(0.3f);
        characterController.flipY();
    }

    public override void die()
    {
        Debug.Log("Player.die");
        playerActions.Death();
        Destroy(gameObject.GetComponent<BoxCollider>());
        base.die();
        sceneKeeper.endGame();
    }

    public override void hit(float force)
    {
        playerActions.Hit(force);
        base.hit(force); 
    }

    public bool eat(float _hunger)
    {
        return hunger.eat(_hunger);
    }

    public bool heal()
    {
        return health.heal();
    }

    //public void crouch(bool isCrouching)
    //{
    //    characterController.crouch(isCrouching);
    //}

}
