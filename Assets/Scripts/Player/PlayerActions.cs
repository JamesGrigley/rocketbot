﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (Animator))]
public class PlayerActions : MonoBehaviour {

	private Animator animator;

	const int countOfDamageAnimations = 3;
	int lastDamageAnimation = -1;

    public AudioClip[] woodChopSounds;
    public AudioClip[] digSounds;

    public AudioClip splash;
    public AudioClip reel;

    AudioSource audioSource;

    PlayerTools playerTools;

    SoundManager soundManager;


    CharacterController3D characterController;

	void Awake () {
		animator = GetComponent<Animator> ();
        audioSource = GetComponent<AudioSource>();
    }

    void Start()
    {
        characterController = GameObject.FindGameObjectWithTag("Player").GetComponent<CharacterController3D>();
        playerTools = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerTools>();
        soundManager = GameObject.Find("SoundManager").GetComponent<SoundManager>();
        //StartCoroutine(test());
    }

    IEnumerator test()
    {
        for (int x = 0; x < 10; x++)
        {
            yield return new WaitForSeconds(3);
        }



    }

    public void Stay () {
		//animator.SetBool("Aiming", false);
		animator.SetFloat ("speed", 0f);
		}

	public void Walk (float speed) {
        //Debug.Log("Walk(" + speed + ")");
        animator.SetFloat ("speed", speed);
	}

    public void beginLoot(float lootPosition, int rotate, float duration)
    {
        //Debug.Log("Loot(" + lootPosition + ", " + rotate + ")");
        if (lootPosition <= 1)
        {
            animator.SetTrigger("loot");
            animator.SetFloat("lootPosition", lootPosition);
            StartCoroutine(stutterAnimation(duration));

        }
        else if (lootPosition == 2) // wood
        {
            animator.SetBool("chopping", true);
            StartCoroutine(stopChop(duration));
            StartCoroutine(chopSounds(duration));
        }
        else if (lootPosition == 3) // dig
        {
            animator.SetBool("digging", true);
            StartCoroutine(doDig(duration));
            StartCoroutine(doDigSounds(duration));
        }
        else if (lootPosition == 4) // hunt
        {
            //ShootBow();
            Debug.Log("//beginLoot.ShootBow();");
        }
        else if (lootPosition == 5) // fish
        {
            fish(duration);
        }

    }

    float digTime = 5;
    IEnumerator doDig(float duration)
    {
        while (duration > 0)
        {
            if (duration >= digTime)
            {
                animator.SetBool("digging", true);
                yield return new WaitForSeconds(digTime);

                animator.SetBool("digging", false);
                yield return new WaitForSeconds(0.001f);
            }

            duration -= digTime;
        }

    }

    IEnumerator doDigSounds(float duration)
    {
        while (duration > 0)
        {
            if (duration >= digTime)
            {
                yield return new WaitForSeconds(0.6f);
                audioSource.PlayOneShot(digSounds[Random.Range(0, digSounds.Length - 1)]);
                yield return new WaitForSeconds(1.4f);
                audioSource.PlayOneShot(digSounds[Random.Range(0, digSounds.Length - 1)]);
                yield return new WaitForSeconds(1.4f);
                audioSource.PlayOneShot(digSounds[Random.Range(0, digSounds.Length - 1)]);
                yield return new WaitForSeconds(1.4f);
                audioSource.PlayOneShot(digSounds[Random.Range(0, digSounds.Length - 1)]);

            }

            duration -= digTime;
        }

    }


    float swingTime = 3.5f;
    IEnumerator stopChop(float duration)
    {
        while (duration > 0)
        {
            if (duration > swingTime)
            {
                animator.SetBool("chopping", true);
                yield return new WaitForSeconds(swingTime);

                animator.SetBool("chopping", false);
                yield return new WaitForSeconds(0.001f);
            }

            duration -= swingTime;
        }

    }

    IEnumerator chopSounds(float duration)
    {
        while (duration > 0)
        {
            if (duration > swingTime)
            {
                yield return new WaitForSeconds(1);
                audioSource.PlayOneShot(woodChopSounds[Random.Range(0, woodChopSounds.Length - 1)]);
                yield return new WaitForSeconds(2);
                audioSource.PlayOneShot(woodChopSounds[Random.Range(0, woodChopSounds.Length - 1)]);
                yield return new WaitForSeconds(0.7f);
            }

            duration -= swingTime;
        }

    }


    IEnumerator stutterAnimation(float duration)
    {
        float holdSpeed = animator.speed;

        yield return new WaitForSeconds(0.9f);
        animator.speed = -holdSpeed;
        yield return new WaitForSeconds(duration - 1);
        animator.speed = holdSpeed;
    }

    bool isHit = false;
    public void Hit(float hitForce)
    {
        if (!isHit && !isDying)
        {
            isHit = true;
            animator.SetFloat("hitForce", hitForce);
            animator.SetBool("hitFront", true);
            animator.SetTrigger("hit");
            StartCoroutine(endHit());
        }

    }

    IEnumerator endHit()
    {
        yield return new WaitForSeconds(0.1f);
        isHit = false;
    }



    public void Throw (float throwWeight) {
        animator.SetFloat("throwType", throwWeight);
		animator.SetTrigger ("throw");
	}

    bool isDying = false;
	public void Death () {
        if (!isDying)
        {
            isDying = true;
            Debug.Log("Setting death trigger");
            animator.SetTrigger("die");
        }
    }

    bool isChopping = false;
    public void Chop()
    {
        if (!isChopping)
        {
            isChopping = true;
            Debug.Log("Setting isChopping trigger");
            animator.SetTrigger("chop");
        }
    }


    void fish(float duration)
    {
        StartCoroutine(doFish(duration));
    }

    IEnumerator doFish(float duration)
    {
        soundManager.playAfterDelay(SoundClipTypeEnum.Whip, audioSource, 1.2f);
        soundManager.playAfterDelay(SoundClipTypeEnum.Splash, audioSource, 2.2f);
        soundManager.playForDuration(SoundClipTypeEnum.Reel, transform, duration - 3.2f, 3.3f);
        soundManager.playAfterDelay(SoundClipTypeEnum.FishSplash, audioSource, duration - 5);
        animator.SetBool("isFishing", true);
        yield return new WaitForSeconds(duration - 0.5f);
        animator.SetBool("isFishing", false);

    }

}
