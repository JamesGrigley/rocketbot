﻿using UnityEngine;
using System.Collections;

public class PlayerPower : MonoBehaviour {

    Player player;
    Sky sky;

    public bool isPowered = true;

    public float maxEnergy = 100;
    public float currentEnergy = 100;
    public float energyPerSecond = 0;

    float engergyWalking = -4;
    float engergyWorking = -8;

    float energySunDawn = 1;
    float energySunDay = 2;
    float energySunDusk = 1;
    float energySunNight = 0;

    public float energyPercent
    {
        get
        {
            return currentEnergy / maxEnergy;
        }
    }

    // Use this for initialization
    void Start () {
        player = GetComponent<Player>();
        sky = GameObject.Find("Sky").GetComponent<Sky>();
    }

    // Update is called once per frame
    void Update () {
        float newEnergyPersecond = 0;
        
        newEnergyPersecond += player.isWalking ? engergyWalking : 0;
        newEnergyPersecond += sky.currentPhase == Sky.DayPhase.Dawn ? energySunDawn : 0;
        newEnergyPersecond += sky.currentPhase == Sky.DayPhase.Day ? energySunDay : 0;
        newEnergyPersecond += sky.currentPhase == Sky.DayPhase.Dusk ? energySunDusk : 0;
        newEnergyPersecond += sky.currentPhase == Sky.DayPhase.Night ? energySunNight : 0;
        energyPerSecond = newEnergyPersecond;

        currentEnergy = Mathf.Clamp(currentEnergy + (energyPerSecond * Time.deltaTime), 0, maxEnergy);

        if (isPowered && currentEnergy == 0)
        {
            isPowered = false;
        }
        else if (!isPowered && currentEnergy > 10)
        {
            isPowered = true;
            sky.worldSpeed = 1;
        }

        if (!isPowered && sky.worldSpeed < 10)
        {
            sky.worldSpeed += 1;
        }

    }
}
