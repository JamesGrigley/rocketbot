﻿using UnityEngine;
using System.Collections;

public class PlayerShoot : MonoBehaviour
{
    public Transform bullet;
    public Transform bulletOrigin;

    PlayerActions playerActions;
    Gun gun;

    bool isShooting = false;
    float shotDelay = 0.2f;


    void Awake()
    {
        playerActions = GetComponent<PlayerActions>();
        //gun = GameObject.Find("AK").GetComponent<Gun>();
    }

    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            shoot();
        }
    }

    void shoot()
    {
        if(!isShooting)
        {
            isShooting = true;
            StartCoroutine(doShot());
        }

    }

    IEnumerator doShot()
    {

        Transform bulletTranform = (Transform)Instantiate(bullet, bulletOrigin.position, bulletOrigin.rotation);
        bulletTranform.GetComponent<BaseBullet>().shooter = gameObject;
        gun.shoot();
        yield return new WaitForSeconds(shotDelay);
        isShooting = false;
    }
}
