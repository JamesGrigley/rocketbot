﻿using UnityEngine;
using System.Collections;

public class PlayerTools : MonoBehaviour
{
    Transform rightHand;
    Transform leftHand;

    Transform axe;
    Transform shovel;
    Transform bow;
    public Transform arrow;
    public Transform arrowPrefab;
    Transform fishingPole;
    GameObject currentTool;

    void Awake()
    {
        rightHand = GameObject.Find("Bip02 Rhand_Weapon").transform;
        leftHand = GameObject.Find("Bip02 shield_mount").transform;

        loadTools();
    }

    public Transform setTool(float lootPosition)
    {
        unsetTool();
        if (lootPosition == 2)
        {
            currentTool = axe.gameObject;
            axe.gameObject.SetActive(true);
        }
        else if (lootPosition == 3)
        {
            currentTool = shovel.gameObject;
            shovel.gameObject.SetActive(true);
        }
        else if (lootPosition == 4)
        {
            currentTool = bow.gameObject;
            arrow = (Transform)Instantiate(arrowPrefab);//, .position, new Quaternion());
            arrow.SetParent(bow.transform.GetChild(0).GetChild(0));
            arrow.localPosition = Vector3.zero;
            arrow.localScale = Vector3.one;
            arrow.gameObject.SetActive(false);
            bow.gameObject.SetActive(true);
        }
        else if (lootPosition == 5)
        {
            currentTool = fishingPole.gameObject;
            fishingPole.gameObject.SetActive(true);
        }

        if (currentTool != null)
            return currentTool.transform;
        else
            return null;

    }

    public void unsetTool()
    {
        if (currentTool != null)
            currentTool.SetActive(false);
    }

    void loadTools()
    {
        axe = (Transform)Instantiate(Resources.Load<Transform>("Prefabs/axe"), rightHand.position, rightHand.rotation);
        axe.parent = rightHand;
        axe.gameObject.SetActive(false);

        shovel = (Transform)Instantiate(Resources.Load<Transform>("Prefabs/shovel"), rightHand.position, rightHand.rotation);
        shovel.parent = rightHand;
        shovel.gameObject.SetActive(false);

        bow = (Transform)Instantiate(Resources.Load<Transform>("Prefabs/bow"), leftHand.position, leftHand.rotation);
        bow.parent = leftHand;
        bow.gameObject.SetActive(false);

        arrowPrefab = Resources.Load<Transform>("Prefabs/arrow");

        fishingPole = (Transform)Instantiate(Resources.Load<Transform>("Prefabs/FishingPole"), rightHand.position, rightHand.rotation);
        fishingPole.parent = rightHand;
        fishingPole.gameObject.SetActive(false);

    }
}
