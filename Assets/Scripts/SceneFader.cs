﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class SceneFader : MonoBehaviour
{
    public float fadeSpeed = 1.5f;          // Speed that the screen fades to and from black.


    bool isStarsMoving = false;
    bool isLogoMoving = false;
    bool isRBLogoMoving = false;
    bool isShipMoving = false;
    bool isStarsMovingDown = false;

    Vector3 logoMovePosition;
    Vector3 shipMovePosition;

    Image faderImage;
    Image faderImage2;
    Image faderImage3;
    Image logoImage;
    Image logoRB;
    Text logoText;

    Camera camera;

    ParticleSystem stars;
    ParticleSystem stars2;
    ShipMotor shipMotor;

    List<Graphic> fadingImages;

    AudioSource audioSource;

    public AudioClip music;
    public AudioClip flyBySound;

    void Awake()
    {
        fadingImages = new List<Graphic>();
        faderImage = GameObject.Find("FaderImage").GetComponent<Image>();
        faderImage2 = GameObject.Find("FaderImage2").GetComponent<Image>();

        logoImage = GameObject.Find("LogoImage").GetComponent<Image>();
        logoText = GameObject.Find("LogoText").GetComponent<Text>();

        camera = GameObject.Find("Camera").GetComponent <Camera>();
        logoMovePosition = logoText.rectTransform.localPosition - (Vector3.up * 800);

        stars = GameObject.Find("Stars").GetComponent<ParticleSystem>();
        stars2 = GameObject.Find("Stars3").GetComponent<ParticleSystem>();
        stars2.gameObject.SetActive(false);

        shipMotor = GameObject.Find("Ship").GetComponent<ShipMotor>();
        shipMovePosition = shipMotor.transform.position + Vector3.left * 150;

        logoRB = GameObject.Find("Canvas/RBLogo/Image").GetComponent<Image>();

        audioSource = GetComponent<AudioSource>();

        StartCoroutine(fading());
    }

    void Start()
    {
        shipMotor.setMainThrusters(100, false);
    }

    void FixedUpdate()
    {
        FadeImages();
        moveLogo();
        spinStars();
        moveShip();
    }

    IEnumerator fading()
    {
        Time.timeScale = 1f;
        yield return new WaitForSeconds(2);
        fadingImages.Add(faderImage);
        yield return new WaitForSeconds(1f);
        audioSource.PlayOneShot(music);
        yield return new WaitForSeconds(3f);
        isLogoMoving = true;
        fadingImages.Add(logoImage);
        fadingImages.Add(logoText);
        fadingImages.Add(faderImage2);
        yield return new WaitForSeconds(0.5f);
        isStarsMoving = true;
        yield return new WaitForSeconds(3);
        isStarsMoving = false;
        yield return new WaitForSeconds(3);
        audioSource.PlayOneShot(flyBySound);
        yield return new WaitForSeconds(3);
        Debug.Log("Moving ship");
        
        isShipMoving = true;
        //yield return new WaitForSeconds(1.5f);
        Time.timeScale = 1.0f;
        yield return new WaitForSeconds(6);
        isStarsMovingDown = true;
        isRBLogoMoving = true;
        //stars.gameObject.SetActive(false);
        //stars2.gameObject.SetActive(true);
        //fadingImages.Add(faderImage3);
        //Application.LoadLevel("game");
    }

    void FadeImages()
    {
        foreach(Graphic image in fadingImages)
        {
            if (image != null)
                image.CrossFadeAlpha(0, fadeSpeed, false);
        }
    }

    float currentLogoSpeed = 0;
    void moveLogo()
    {
        if(isLogoMoving)
        {
            currentLogoSpeed = Mathf.Clamp(currentLogoSpeed + Time.deltaTime * 5f, 0, 50);
            logoText.rectTransform.Translate(-Vector3.up * currentLogoSpeed);
            logoImage.rectTransform.Translate(-Vector3.up * currentLogoSpeed);

            //logoText.rectTransform.localPosition = Vector3.SmoothDamp(logoText.rectTransform.localPosition, logoMovePosition, ref currentVelocity, 3);
            //logoImage.rectTransform.localPosition = Vector3.SmoothDamp(logoImage.rectTransform.localPosition, logoMovePosition, ref currentVelocity, 3);

        }

        if (isRBLogoMoving)
        {
            currentLogoSpeed = Mathf.Clamp(currentLogoSpeed + Time.deltaTime * 100f, 0, 50);
            logoRB.rectTransform.Translate(Vector3.up * currentLogoSpeed);
        }
    }


    float currentStarSpeed = 0;
    void spinStars()
    {
        if(isStarsMoving)
        {
            stars.gravityModifier = currentStarSpeed;
            currentStarSpeed = Mathf.Clamp(currentStarSpeed + Time.deltaTime * 100f, 0, 120);

        }
        if (isStarsMovingDown)
        {
            stars.gravityModifier = currentStarSpeed;
            currentStarSpeed = Mathf.Clamp(currentStarSpeed - Time.deltaTime * 15f, -120, 0);

        }
        else
        {
            stars.gravityModifier = currentStarSpeed;
            currentStarSpeed = Mathf.Clamp(currentStarSpeed - Time.deltaTime * 60f, 0, 80);
        }

    }

    
    void moveShip()
    {
        if (isShipMoving)
            shipMotor.transform.position = Vector3.Lerp(shipMotor.transform.position, shipMovePosition, 0.04f);
    }


}