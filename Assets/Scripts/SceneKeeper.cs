﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class SceneKeeper : MonoBehaviour {

    GameOver gameOver;

    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
        DontDestroyOnLoad(GameObject.FindGameObjectWithTag("Player"));
        DontDestroyOnLoad(GameObject.Find("Components"));
        DontDestroyOnLoad(GameObject.Find("UI"));
        DontDestroyOnLoad(GameObject.Find("Ship"));

    }

    void Start()
    {
        gameOver = GameObject.Find("GameOver").GetComponent<GameOver>();
        gameOver.gameObject.SetActive(false);
    }

    public void reset()
    {
        Debug.Log("reset");
        SceneManager.UnloadScene(SceneManager.GetActiveScene().name);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
        Time.timeScale = 1;

    }

    public void endGame()
    {
        Debug.Log("SceneKeeper End Game");
        gameOver.gameObject.SetActive(true);
        if (gameOver.endGame())
        {
            GameObject.Find("Canvas").gameObject.SetActive(false);
            GameObject.Find("Canvas2").gameObject.SetActive(false);
        }
    }

}
