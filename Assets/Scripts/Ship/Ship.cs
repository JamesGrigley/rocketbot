﻿using UnityEngine;
using System.Collections;

public class Ship : MonoBehaviour
{
    ShipWindow shipWindow;
    ShipMotor shipMotor;

    public float currentFuel = 0;
    public float maxFuel = 100;

    public Icon icon;

    public bool isDamaged = true;

    void Awake()
    { 
        shipMotor = GetComponent<ShipMotor>();
        shipWindow = GetComponent<ShipWindow>();
    }

    void Start()
    {
        icon = IconSpawner.spawnIcon();
        icon.setIcon(IconTypeEnum.Broken, 0, this.transform, clickable, null, clickShip);
    }

    public void clickShip()
    {
        //Debug.Log("clicking the ship?");
        shipWindow.activate();
    }

    public void startRepair()
    {
        icon.activationTime = 5;
        icon.activateFinishedCallBackDelegate = finishedRepair;
        icon.activate();

    }

    public void finishedRepair()
    {
        icon.setIcon(IconTypeEnum.Inspect, 0, this.transform, clickable, null, clickShip);
        isDamaged = false;

    }

    public bool clickable()
    {
        return true;
    }
}