﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShipMotor : MonoBehaviour {

    public AudioClip[] audioClips;
    AudioSource audioSource;

    List<ParticleSystem> enginesLift;
    List<ParticleSystem> enginesMain;

    Rigidbody rb;

    float maxMainThrust = 100;
    float currentMainThrust = 0;

    float liftVelocity = 0;

    float maxLiftThrust = 50;
    float hoverLiftThrust = 50;
    float lowerLiftThrust = 10;
    float currentLiftThrust = 0;
    float actualLiftThrust = 0;

    // Use this for initialization
    void Awake () {
        enginesLift = new List<ParticleSystem>();
        enginesMain = new List<ParticleSystem>();

        rb = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
        loadParticleSystems();
        setSecondaryThrusters(0);
        setMainThrusters(0);
        //
    }

    // Update is called once per frame
    void Update () {
        actualLiftThrust = Mathf.SmoothDamp(actualLiftThrust, currentLiftThrust, ref liftVelocity, 4);

        if (actualLiftThrust > 0)
            rb.AddForce(transform.up * actualLiftThrust);

        if (currentMainThrust > 0)
            rb.AddForce(transform.forward * currentMainThrust);
    }

    public void startEngine()
    {
        // start engines (sound only)
        audioSource.PlayOneShot(audioClips[0]);
    }

    public void liftToHover()
    {
        // set left thrusters
        setSecondaryThrusters(hoverLiftThrust);
        audioSource.PlayOneShot(audioClips[1]);
    }

    public void liftLower()
    {
        // set left thrusters
        setSecondaryThrusters(lowerLiftThrust);
        audioSource.PlayOneShot(audioClips[1]);
    }

    public void liftToMax()
    {
        // set left thrusters
        setSecondaryThrusters(maxLiftThrust);
        audioSource.PlayOneShot(audioClips[2]);
    }

    public void liftStop()
    {
        // set left thrusters
        setSecondaryThrusters(0);
    }

    public void thrustMax()
    {
        setMainThrusters(maxMainThrust);
        audioSource.PlayOneShot(audioClips[3]);
    }

    void loadParticleSystems()
    {
        enginesLift.Add(transform.FindChild("LiftEngine1").GetComponent<ParticleSystem>());
        enginesLift.Add(transform.FindChild("LiftEngine2").GetComponent<ParticleSystem>());

        enginesMain.Add(transform.FindChild("MainEngine").GetComponent<ParticleSystem>());
        enginesMain.Add(transform.FindChild("SecondaryEngine1").GetComponent<ParticleSystem>());
        enginesMain.Add(transform.FindChild("SecondaryEngine2").GetComponent<ParticleSystem>());
    }

    public void setMainThrusters(float thrust, bool doThrust = true)
    {
        if (doThrust)
            currentMainThrust = thrust;

        foreach (ParticleSystem engine in enginesMain)
        {
            engine.startSpeed = Mathf.Clamp(thrust * 0.5f, 0, 10);
            engine.gameObject.SetActive(thrust > 0 ? true : false);
        }
    }

    void setSecondaryThrusters(float thrust)
    {
        currentLiftThrust = thrust;

        foreach (ParticleSystem engine in enginesLift)
        {
            engine.startSpeed = Mathf.Clamp(thrust * 0.5f, 0, 10);
            engine.gameObject.SetActive(thrust > 0 ? true : false);
        }
    }

}
