﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum ShipRoutinesEnum
{
    StartEngine,
    LiftOff
}

public class ShipRoutines : MonoBehaviour
{
    Ship ship;
    ShipMotor shipMotor;
    SmoothFollow2D camera;
    public ShipRoutinesEnum shipRoutine;
    Player player;

    void Awake()
    {
        ship = GetComponent<Ship>();
        shipMotor = GetComponent<ShipMotor>();
        camera = GameObject.Find("Camera").GetComponent<SmoothFollow2D>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }

    void Start()
    {
        //runRoutine();
    }

    public void runRoutine()
    {
        if (shipRoutine == ShipRoutinesEnum.LiftOff)
        {
            StartCoroutine(liftOffRoutine());
        }
    }

    IEnumerator liftOffRoutine()
    {
        // start motor
        takeFocus();
        yield return new WaitForSeconds(1);
        shipMotor.startEngine();
        yield return new WaitForSeconds(3);


        // lift to hover
        //shipMotor.liftToHover();

        yield return new WaitForSeconds(2);

        //shipMotor.liftLower();


        //// lift to sky
        //shipMotor.liftToMax();

        //yield return new WaitForSeconds(2);

        //// blast off

        //shipMotor.liftStop();
        //yield return new WaitForSeconds(1);
        //shipMotor.thrustMax();
        yield return new WaitForSeconds(4);
        clearFocus();
    }

    Vector3 previousCameraPosition;
    Quaternion previousCamerRotation;

    void takeFocus()
    {
        previousCameraPosition = camera.transform.position;
        previousCamerRotation = camera.transform.rotation;

        ship.icon.toggleVisible(false);
        //player.gameObject.SetActive(false);
        player.gameObject.GetComponent<SpriteRenderer>().enabled = false;

        camera.target = this.transform;
    }

    void clearFocus()
    {
        //player.gameObject.SetActive(true);
        player.gameObject.GetComponent<SpriteRenderer>().enabled = true;
        camera.target = player.transform;
        ship.icon.toggleVisible(true);
        camera.transform.position = previousCameraPosition;
        camera.transform.rotation = previousCamerRotation;
    }
}