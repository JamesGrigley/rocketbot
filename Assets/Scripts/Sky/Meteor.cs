﻿using UnityEngine;
using System.Collections;

public class Meteor : MonoBehaviour {

    public GameObject explosionPrefab;

    void Start()
    {
        //Destroy(this.gameObject, 7);
    }
    void Update()
    {
        if(transform.position.y < 0)
        {
            //explode();
        }
    }

    void OnCollisionEnter(Collision col)
    {
        explode();
    }

    void explode()
    {
        Instantiate(explosionPrefab, transform.position, transform.rotation);
        Destroy(this.gameObject);
    }
}
