﻿using UnityEngine;
using System.Collections;

public class MeteorShower : MonoBehaviour {

    public GameObject meteorPrefab;

    float currentTicks = 1;
    float ticketBetweenMeteor = .5f;

    ShipMotor shipMotor;


	// Use this for initialization
	void Awake () {
        //shipMotor = GameObject.Find("Ship").GetComponent<ShipMotor>();

        //for (int x = 0; x < 500; x++)
        //{
        //    currentTicks += Random.Range(0.5f, 1);

        //    StartCoroutine(launchMeteor(x));
        //}
        //StartCoroutine(launchDeathMeteor());

    }

    // Update is called once per frame
    void Update () {
	
	}

    IEnumerator launchMeteor(int x)
    {
        yield return new WaitForSeconds(currentTicks);

        Vector2 randomVector = Random.insideUnitCircle * 50;
        Vector3 randomPosition = new Vector3(randomVector.x, 0, randomVector.y);

        Rigidbody meteorRB;
        meteorRB = ((GameObject)Instantiate(meteorPrefab, this.transform.position + randomPosition, this.transform.rotation)).GetComponent<Rigidbody>();
        meteorRB.transform.Rotate(new Vector3(Random.Range(-40, 40), Random.Range(-40, 40), Random.Range(-40, 40)));
        meteorRB.velocity = meteorRB.transform.TransformDirection(0, Random.Range(-50, -20), 0);

    }

    IEnumerator launchDeathMeteor()
    {
        yield return new WaitForSeconds(2);

        //Rigidbody meteorRB;
        //meteorRB = ((GameObject)Instantiate(meteorPrefab, this.transform.position, this.transform.rotation)).GetComponent<Rigidbody>();
        ////meteorRB.transform.LookAt(shipMotor.transform.position);
        ////meteorRB.AddRelativeForce(Vector3.forward * 500);
        //meteorRB.lo

        GameObject meteorGO = (GameObject)Instantiate(meteorPrefab, this.transform.position, this.transform.rotation);
        Transform meteorT = meteorGO.transform;
        meteorT.LookAt(shipMotor.transform.position);

    }
}
