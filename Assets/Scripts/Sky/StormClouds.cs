﻿using UnityEngine;
using System.Collections;

public class StormClouds : MonoBehaviour
{
    SoundManager soundManager;

    GameObject lightning;
    GameObject rain;

    float nextLightning = 3;

    float lightningBeginTime = 60;
    float rainBeginTime = 90;

    bool isLightning = false;
    bool isRaining = false;

    void Awake()
    {
        lightning = transform.Find("Lightning").gameObject;
        lightning.SetActive(false);

        rain = transform.Find("Rain").gameObject;
        rain.SetActive(false);
    }

    void Start()
    {
        soundManager = GameObject.Find("SoundManager").GetComponent<SoundManager>();
    }

    void Update()
    {

        if (!isRaining)
        {
            rainBeginTime -= Time.deltaTime;
            if (rainBeginTime <= 0)
            {
                isRaining = true;
                rain.SetActive(true);
            }
        }

        if (!isLightning)
        {
            lightningBeginTime -= Time.deltaTime;
            if (lightningBeginTime <= 0)
            {
                isLightning = true;
            }
        }
        else
        {
            nextLightning -= Time.deltaTime;
            if (nextLightning < 0)
            {
                nextLightning = Random.Range(6, 12);
                StartCoroutine(doLightning());

            }

        }
        //transform.Rotate(transform.forward * Time.deltaTime);
    }

    IEnumerator doLightning()
    {
        lightning.SetActive(true);
        yield return new WaitForSeconds(Random.Range(0.2f, 0.5f));
        soundManager.playForDuration(SoundClipTypeEnum.Thunder, null, 0, 0);
        yield return new WaitForSeconds(Random.Range(0.6f, 1.5f));
        lightning.SetActive(false);
    }

}