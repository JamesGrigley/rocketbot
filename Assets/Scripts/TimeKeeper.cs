﻿using UnityEngine;
using System.Collections;
using System;

public class TimeKeeper : MonoBehaviour {

    public string currentDateTime
    {
        get { return new DateTime(currentTime).ToString("MMMM dd, yyyy hh:mm:ss tt"); }
        //get
        //{
        //    return currentTime.ToString();
        //}
    }

    private long currentTime;

    private float lastTime;

    float realtimeMultiplier;
    //10,000,000,000,000 too fast
    // 5,000,000,000,000 too fast

    // 1,000,000,000,000 not nearly fast enough
    // Use this for initialization
    void Start () {
        currentTime = DateTime.Parse("5/8/2042 11:00").Ticks;

        lastTime = Time.time;
        realtimeMultiplier = TimeSpan.TicksPerHour;

        //currentTime += TimeSpan.TicksPerHour;
    }
	
	// Update is called once per frame
	void Update () {
        float gameTicks = (Time.time - lastTime) * realtimeMultiplier;

        currentTime += Convert.ToInt64(gameTicks);

        //currentTime = currentTime + TimeSpan.TicksPerMinute;

        ////Debug.Log("Adding " + (Time.time - lastTime) * realtimeMultiplier + " ticks");
        lastTime = Time.time;
    }
}
