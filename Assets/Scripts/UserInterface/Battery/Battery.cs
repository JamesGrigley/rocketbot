﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class Battery : MonoBehaviour
{
    public Image batteryChargeImage;

    public void Awake()
    {
        batteryChargeImage = transform.FindChild("Forground").GetComponent<Image>();
    }
}