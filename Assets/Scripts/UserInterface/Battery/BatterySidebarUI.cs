﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BatterySidebarUI : MonoBehaviour {

    PlayerPower playerPower;
    public GameObject meterBar;

    Image posBar;
    Image negBar;

    float lastEnergyPerSecond;

    void Awake()
    {
        playerPower =  GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerPower>();
        posBar = transform.FindChild("Pos").GetComponent<Image>();
        negBar = transform.FindChild("Neg").GetComponent<Image>();
    }

    // Use this for initialization
    void Start () {
    }


    // Update is called once per frame
    void Update() {

        float energyToShow = playerPower.energyPerSecond;

        posBar.fillAmount = Mathf.Clamp((energyToShow * 0.2f) , 0, 1);
        negBar.fillAmount = Mathf.Clamp(-(energyToShow * 0.2f) , 0, 1);
    }

    //IEnumerator showBars()
    //{
    //    int currentEnergy = Mathf.FloorToInt(playerPower.energyPerSecond);
    //    int x = 0;

    //    while (Mathf.Abs(currentEnergy) >  Mathf.Abs(x))
    //    {

    //        x += currentEnergy > 0 ? 1 : -1;
    //    }

    //    yield return null;
    //}

    //void createBars()
    //{

    //}

    //void createPositiveBars()
    //{
    //    for 
    //}
}
