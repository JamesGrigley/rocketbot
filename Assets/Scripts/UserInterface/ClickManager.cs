﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public static class WindowManager
{
    public static bool isWindowPopped;
}

public class ClickManager : MonoBehaviour {

    private Camera camera;
    Player player;
    public Transform debugClickPrefab;

    public bool effects = false;

    public Color[] colors;
    int currentColorIndex = 0;

    // Use this for initialization
    void Start () {
        camera =  GameObject.Find("Camera").GetComponent<Camera>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();

    }

    // Update is called once per frame
    void Update() {
        if (!WindowManager.isWindowPopped && Time.timeScale > 0 && Input.GetMouseButton(0) && !EventSystem.current.IsPointerOverGameObject())
        {
            handleMouseClick();

        }

        if (Input.GetMouseButtonDown(1))
        {
            player.GetComponent<ShootBow>().execute();
        }

        handleViewportSelection();
    }

    void handleViewportSelection()
    {
        int x = Screen.width / 2;
        int y = Screen.height / 2;

        RaycastHit hit;
        Ray ray = camera.ScreenPointToRay(new Vector3(x, y));
        int LayerID = 1 << 8;

        Debug.DrawRay(ray.origin, ray.direction);


        if (Physics.Raycast(ray, out hit, Mathf.Infinity))
        {
            if(hit.collider.tag == "Lootable" && hit.collider.GetComponent<BaseContainer>() != null)
            {
                hit.collider.GetComponent<BaseContainer>().icon.select();
            }
        }
    }

    void handleMouseClick()
    {
        int x = Screen.width / 2;
        int y = Screen.height / 2;

        RaycastHit hit;
        Ray ray = camera.ScreenPointToRay(new Vector3(x, y));
        int LayerID = 1 << 8; // need lootable layer

        Debug.DrawRay(ray.origin, ray.direction);


        if (Physics.Raycast(ray, out hit, Mathf.Infinity))
        {
            //
            if (hit.collider.tag == "Lootable" && hit.collider.GetComponent<BaseContainer>() != null)
            {
                hit.collider.GetComponent<BaseContainer>().icon.iconClick();
            }
        }


    }
}
