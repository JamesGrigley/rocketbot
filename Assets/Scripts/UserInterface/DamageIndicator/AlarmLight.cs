﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AlarmLight : MonoBehaviour {

    float fadSpeed = 5;
    float highIntensity = 10;
    float lowIntensity = 0;

    float changeMargin = 0.2f;
    public bool alarmOn;
    public bool soundOn = true;

    float targetIntensity;

    float intensity;

    Image image;
    AudioSource audioSource;


    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        intensity = highIntensity;
        targetIntensity = lowIntensity; //starting at high, going to low
        image = GetComponent<Image>();
    }

    void Update()
    {
        if (alarmOn)
        {
            intensity = Mathf.Lerp(intensity, targetIntensity, fadSpeed * Time.deltaTime);
            CheckTargetIntensity();
        }
        else
        {
            intensity = Mathf.Lerp(intensity, highIntensity, fadSpeed * Time.deltaTime);
        }

        Color color = image.color;
        color.a = intensity;
        image.color = color;
    }

    void CheckTargetIntensity()
    {
        //if (Mathf.Abs(targetIntensity - intensity) < changeMargin)
        if (Mathf.Abs(targetIntensity - intensity) <= changeMargin)
        {

            if (targetIntensity == highIntensity)
            {
                targetIntensity = lowIntensity;
            }
            else
            {
				if (soundOn && audioSource != null)
                    audioSource.Play(1);
                targetIntensity = highIntensity;
            }
        }
    }
}
