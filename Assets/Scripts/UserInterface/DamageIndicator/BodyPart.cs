﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public enum BodyPartType
{
    Head,
    Body,
    LeftArm,
    RightArm,
    LeftLeg,
    RightLeg
}

public enum DamageState
{
    None,
    Moderate,
    Severe
}

public class BodyPart : MonoBehaviour {

    public BodyPartType bodyPartType;
    Image image;
    public DamageState damageState;

    public AlarmLight alarm;

    DamageIndicator damageIndicator;

    void Awake()
    {
        alarm = GetComponent<AlarmLight>();
        image = GetComponent<Image>();
        damageIndicator = GameObject.Find("DamageIndicator").GetComponent<DamageIndicator>();
    }

    public void setDamage(DamageState _damageState)
    {
        damageState = _damageState;
        //setColor();

    }

    public void click()
    {
        alarm.soundOn = false;
        damageIndicator.damageWindow.activate(this);
    }

    void setColor()
    {
        if (damageState == DamageState.None)
        {
            image.color = Color.green;
            alarm.alarmOn = false;
        }
        else if (damageState == DamageState.Moderate)
        {
            image.color = Color.yellow;
            alarm.alarmOn = false;
        }
        else if (damageState == DamageState.Severe)
        {
            image.color = Color.red;
            alarm.alarmOn = true;
        }
    }

}
