﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class DamageIndicator : MonoBehaviour {

    public DamageWindow damageWindow;
    
    List<BodyPart> bodyParts;

    // Use this for initialization
    void Awake () {
        damageWindow = GetComponent<DamageWindow>();
        initParts();
        setAllParts(DamageState.None);
        //setPart(BodyPartType.Head, DamageState.Severe);
    }

    // Update is called once per frame
    void Update () {
	
	}


    void initParts()
    {
        bodyParts = new List<BodyPart>()
        {
            transform.FindChild("Head").GetComponent<BodyPart>(),
            transform.FindChild("Body").GetComponent<BodyPart>(),
            transform.FindChild("LeftArm").GetComponent<BodyPart>(),
            transform.FindChild("RightArm").GetComponent<BodyPart>(),
            transform.FindChild("LeftLeg").GetComponent<BodyPart>(),
            transform.FindChild("RightLeg").GetComponent<BodyPart>()
        };
    }

    void setAllParts(DamageState damageState)
    {
        foreach(BodyPart bodyPart in bodyParts)
        {
            bodyPart.setDamage(damageState);
        }
    }

    void setPart(BodyPartType bodyPartType, DamageState damageState)
    {
        foreach (BodyPart bodyPart in bodyParts)
        {
            if (bodyPart.bodyPartType == bodyPartType)
            {
                bodyPart.setDamage(damageState);
                return;
            }
        }
    }
}
