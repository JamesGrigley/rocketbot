﻿using UnityEngine;
using System.Collections;
using System;

public class DeathButton : BaseButton {

    public override void click()
    {
        Application.LoadLevel(Application.loadedLevelName);
        Time.timeScale = 1;
    }
}
