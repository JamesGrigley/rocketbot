﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class DialogManager : MonoBehaviour
{
    static GameObject speechBubblePrefab;

    void Awake()
    {
        speechBubblePrefab = (GameObject)Resources.Load("Prefabs/UI/SpeechBubble");
    }

    void Start()
    {
        //StartCoroutine(test()); 

    }

    IEnumerator test()
    {
        yield return new WaitForSeconds(3);
        spawnSpeechBubble(GameObject.FindGameObjectWithTag("Player").transform, "Your mother said hi!");

    }

    public static SpeechBubble spawnSpeechBubble(Transform target, string text)
    {
        SpeechBubble speechBubble;

        Transform speechBubbleTransform;
        speechBubbleTransform = Instantiate(speechBubblePrefab.transform) as Transform;
        speechBubbleTransform.SetParent(GameObject.Find("DialogCanvas").transform, false);

        speechBubble = speechBubbleTransform.GetComponent<SpeechBubble>();

        speechBubble.text = text;
        speechBubble.target = target;

        return speechBubble;
    }
}