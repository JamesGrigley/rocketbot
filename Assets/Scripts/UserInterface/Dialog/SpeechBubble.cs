﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class SpeechBubble : MonoBehaviour
{

    Text textUI;
    Image bubbleImage;

    FirstPersonController firstPersonController;

    public string text;

    public Transform target;
    Camera camera;

    DialogStatusEnum dialogStatus;

    float activationTime = 2;

    enum DialogStatusEnum
    {
        Activating,
        Showing,
        Deactivating,
        Dead
    }

    void Awake()
    {
        camera = GameObject.Find("Camera").GetComponent<Camera>();
        bubbleImage = transform.FindChild("Bubble").GetComponent<Image>();
        textUI = transform.FindChild("Bubble/Text").GetComponent<Text>();
    }

    void Start()
    {
        firstPersonController = GameObject.FindGameObjectWithTag("Player").GetComponent<FirstPersonController>();
        reposition();

        dialogStatus = DialogStatusEnum.Activating;
        textUI.text = text;
        StartCoroutine(delayedStart());
        //Destroy(gameObject, 10);

    }

    IEnumerator delayedStart()
    {
        yield return new WaitForSeconds(4);
        firstPersonController.SetLock(true);
    }

    void Update()
    {
        reposition();
        //if (dialogStatus == DialogStatusEnum.Activating)
        //{
        //    fadeIn();
        //}
    }

    void reposition()
    {
        if (target != null)
        {
            transform.position = camera.WorldToScreenPoint(target.position);
        }

    }

    float currentFadeTime = -1;
    void fadeIn()
    {
        if (currentFadeTime == -1)
        {
            currentFadeTime = activationTime;
        }else if(currentFadeTime < 0)
        {
            dialogStatus = DialogStatusEnum.Showing;
            currentFadeTime = -1;
            return;
        }
        bubbleImage.CrossFadeAlpha(1, currentFadeTime, false);

        currentFadeTime -= Time.deltaTime;
    }

    public void choiceClick(int choice)
    {
        if (choice == 1)
        {
            Debug.Log("I clicked yes");
        }
        else
        {
            Debug.Log("I clicked no");
        }
        firstPersonController.SetLock(false);

        Destroy(gameObject);
    }
}
