﻿using UnityEngine;
using System.Collections;

public class DialogPanelUI : MonoBehaviour {

    Rect windowRect;

    int windowWidth = 350;
    int windowHeight = 250;

    AudioSource audioSource;

    bool showWindow = false;

    private int slideState = 0;
    private float slideStartTime;
    private float slideDuration = 3f;


    private Vector2 panelShowPosition;
    private Vector2 panelHidePosition;

    // Use this for initialization
    void Start () {
        windowRect = new Rect(Screen.width - windowWidth - 10, 100, windowWidth, windowHeight);
        audioSource = GetComponent<AudioSource>();
        panelShowPosition = windowRect.position;
        panelHidePosition = new Vector2(panelShowPosition.x  + windowWidth * 2, panelShowPosition.y);
        windowRect.position = panelHidePosition;
        //StartCoroutine(testPop());
    }


    // Update is called once per frame
    void Update () {
        lerpMuthaFuckaLerp();
    }

    void OnGUI()
    {
        if (showWindow)
        {
            windowRect = GUI.Window(0, windowRect, DoMyWindow, "3D Printer 800XL Menu");
        }
    }

    void DoMyWindow(int windowID)
    {
        // close button
        if (GUI.Button(new Rect(windowWidth - 30, 20, 20, 20), "X"))
        {
            slideState = 2;
            slideStartTime = Time.time;
        }

        GUI.Label(new Rect(120, 150, 150, 50), "Tet");
    }

    void lerpMuthaFuckaLerp()
    {

        if (slideState == 1 && windowRect.position == panelShowPosition)
        {
            slideState = 0;
        }
        else if (slideState == 2 && windowRect.position == panelHidePosition)
        {
            slideState = 0;
        }

        if (slideState == 1)
        {
            float fracComplete = (Time.time - slideStartTime) / slideDuration;
            windowRect.position = Vector2.Lerp(panelHidePosition, panelShowPosition, fracComplete);
        }

        if (slideState == 2)
        {
            float fracComplete = (Time.time - slideStartTime) / (slideDuration);
            windowRect.position = Vector2.Lerp(panelShowPosition, panelHidePosition, fracComplete);
        }

    }

    void toggleWindow()
    {
        showWindow = !showWindow;
        if (showWindow)
        {
            if (slideState == 0)
            {
                slideState = 1;
                slideStartTime = Time.time;
            }
            //audioSource.Play();

        }
        else
        {
            if (slideState == 0)
            {
                slideState = 2;
                slideStartTime = Time.time;
            }

        }
    }

    IEnumerator testPop()
    {
        yield return new WaitForSeconds(3);
        toggleWindow();

    }


}
