﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public delegate void buttonClickDelegate();
public delegate void buttonClickParmDelegate(System.Object parm);

public static class GUIHelper
{
    public class button
    {
        public string buttonText;
        public buttonClickDelegate buttonClick;

        public button(string _buttonText, buttonClickDelegate _buttonClick)
        {
            buttonText = _buttonText;
            buttonClick = _buttonClick;
        }
    }

    public static void drawCenteredLabel(string labelText)
    {
        GUILayout.FlexibleSpace();

        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        GUILayout.Label(labelText);
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
    }

    public static void drawCenteredButton(string buttonText, buttonClickDelegate _buttonClick)
    {
        GUILayout.FlexibleSpace();

        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        if (GUILayout.Button(buttonText))
        {
            _buttonClick();
        }
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
    }

    public static void drawCenteredButton(Texture buttonTexture, buttonClickDelegate _buttonClick, int width = 200, int height=50)
    {
        GUILayout.FlexibleSpace();

        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        if (GUILayout.Button(buttonTexture, GUILayout.Width(width), GUILayout.Height(height)))
        {
            _buttonClick();
        }
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
    }

    public static void drawButton(Texture buttonTexture, buttonClickParmDelegate _buttonClick, System.Object parm, Vector2 size)
    {
        if (GUILayout.Button(buttonTexture, GUILayout.Width(size.x), GUILayout.Height(size.y)))
        {
            _buttonClick(parm);
        }
    }

    public static void drawCenteredButtons(List<button> buttons)
    {
        GUILayout.FlexibleSpace();

        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        GUILayout.FlexibleSpace();

        foreach(button button in buttons)
        {
            if (GUILayout.Button(button.buttonText))
            {
                button.buttonClick();
            }
            GUILayout.FlexibleSpace();

        }
        GUILayout.FlexibleSpace();

        GUILayout.EndHorizontal();
    }
}