﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public enum IconTypeEnum
{
    Trash = 1,
    Wood = 2,
    Inspect = 3,
    ElectricCharge = 4,
    Broken = 5,
    Shovel = 6,
    Fishing = 7,
    Hunt = 8,
    Campfire
}

public class Icon : MonoBehaviour {

    AudioSource audioSource;
    Animator animator;

    Image iconForeground;
    Image iconBackground;
    Image iconDenied;

    Camera camera;

    Player player;

    public IconTypeEnum iconType = IconTypeEnum.Wood;
    IconTypeEnum lastIconType;

    IconStateEnum iconState;
    enum IconStateEnum
    {
        Inactive,
        Active,
        Selected,
        Clicked,
        IsActivating
    }

    bool visible = true;
    public bool clickable;
    bool lastClickable;
    bool isFadingOut = true;
    float lastFade;

    float selectionTime;

    public Transform target;
    public float activationTime = 5;

    public delegate bool IsClickableDelegate();
    public IsClickableDelegate isClickableDelegate;

    public delegate void ActivateStartCallBackDelegate();
    public ActivateStartCallBackDelegate activateStartCallBackDelegate;

    public delegate void ActivateFinishedCallbackDelegate();
    public ActivateFinishedCallbackDelegate activateFinishedCallBackDelegate;


    void Awake () {

        lastIconType = iconType;
        lastClickable = clickable;
        iconForeground = transform.FindChild("ForgroundImage").GetComponent<Image>();
        iconBackground = transform.FindChild("BackgroundImage").GetComponent<Image>();
        iconDenied = transform.FindChild("DeniedImage").GetComponent<Image>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        camera = GameObject.Find("Camera").GetComponent<Camera>();
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
    }

    void Start()
    {
        loadImages();
    }

    // Update is called once per frame
    void Update()
    {

        if (iconType != lastIconType)
        {
            lastIconType = iconType;
            loadImages();
        }
        if (clickable != lastClickable)
        {
            lastClickable = clickable;
            loadImages();
        }

        if (target != null)
        {
            if (camera != null)
                transform.position =  camera.WorldToScreenPoint(target.position);

            if (visible && Vector3.Distance(target.position, player.transform.position) > 3)
            {
                // LOOKER IS NOT FACING TARGET YET
                toggleVisible(false);
            }
            else if(!visible && Vector3.Distance(target.position, player.transform.position) <= 3)
            {

                toggleVisible(true);
            }

        }
        else
            GameObject.Destroy(this.gameObject);

        if (iconState == IconStateEnum.IsActivating && activationTime > 0)
        {
            iconBackground.fillAmount -= 1.0f / activationTime * Time.deltaTime;

            if (Time.time - lastFade > 0.5f)
            {
                lastFade = Time.time;
                isFadingOut = !isFadingOut;
            }

            iconBackground.CrossFadeAlpha(isFadingOut ? 0 : 1, 0.5f, false);
        }

        

        if (iconState == IconStateEnum.Selected && Time.time - selectionTime > 0.01f)
        {
            iconState = IconStateEnum.Active;
            animator.SetInteger("buttonState", 0);
        }

    }

    public void setIcon(
        IconTypeEnum _iconType,
        float _activationTime,
        Transform _target,
        IsClickableDelegate _isClickableDelegate,
        ActivateStartCallBackDelegate _activateStartCallBackDelegate,
        ActivateFinishedCallbackDelegate _activateFinishedCallBackDelegate)
    {

        iconType = _iconType;
        activationTime = _activationTime;
        isClickableDelegate = _isClickableDelegate;
        activateStartCallBackDelegate = _activateStartCallBackDelegate;
        activateFinishedCallBackDelegate = _activateFinishedCallBackDelegate;
        target = _target;
        target.tag = "Lootable";

        refresh();
        reset();
    }

    public void refresh()
    {
        clickable = isClickableDelegate();
    }

    public void reset()
    {
        iconState = IconStateEnum.Active;
        iconBackground.fillAmount = 1;
        iconBackground.CrossFadeAlpha(1, 1, false);

    }


    public void iconClick()
    {
        if (iconState == IconStateEnum.Selected && Time.timeScale > 0 && !player.isSearching)
        {
            iconState = IconStateEnum.Clicked;

            //Debug.Log("I was clicked");
            audioSource.Play();
            activate();
            //player.goTo(target.position, true, activate);
        }
    }

    public void select()
    {
        selectionTime = Time.time;

        if (iconState == IconStateEnum.Active)
        {
            iconState = IconStateEnum.Selected;
            animator.SetInteger("buttonState", 1);
        }
    }


    public void activate()
    {
        if (clickable)
        {
            StartCoroutine(handleTimedActivation());
        }
            
    }

    IEnumerator handleTimedActivation()
    {

        // wait a second for the player to get into position
        yield return new WaitForSeconds(0.1f);
        iconState = IconStateEnum.IsActivating;

        if (activateStartCallBackDelegate != null)
            activateStartCallBackDelegate();


        yield return new WaitForSeconds(activationTime);

        if (activationTime > 0)
            audioSource.Play();

        if (activateFinishedCallBackDelegate != null)
            activateFinishedCallBackDelegate();


        yield return new WaitForSeconds(0.25f); //small buffer, because sometimes you close the window this opened and the click comes through, this prevents it
        reset();
    }

    void loadImages()
    {
        iconBackground.sprite = Resources.Load<Sprite>("Images/IconBackground");

        if (iconType == IconTypeEnum.Trash)
        {
            iconForeground.sprite = Resources.Load<Sprite>("Images/Icons/64px/Inventory_Moneybag");
        }
        else if (iconType == IconTypeEnum.Wood) {
            iconForeground.sprite = Resources.Load<Sprite>("Images/Icons/64px/Weapons_Axe");
        }
        else if (iconType == IconTypeEnum.Inspect)
        {
            iconForeground.sprite = Resources.Load<Sprite>("Images/Icons/64px/UI_Touch");
        }
        else if (iconType == IconTypeEnum.ElectricCharge)
        {
            iconForeground.sprite = Resources.Load<Sprite>("Images/Icons/64px/Elements_Energy");
        }
        else if (iconType == IconTypeEnum.Broken)
        {
            iconForeground.sprite = Resources.Load<Sprite>("Images/Icons/64px/Tools_Wrench");
        }
        else if (iconType == IconTypeEnum.Shovel)
        {
            iconForeground.sprite = Resources.Load<Sprite>("Images/Icons/64px/Tools_Shovel");
        }
        else if (iconType == IconTypeEnum.Fishing)
        {
            iconForeground.sprite = Resources.Load<Sprite>("Images/Icons/64px/Action_Fishing");
        }
        else if (iconType == IconTypeEnum.Hunt)
        {
            iconForeground.sprite = Resources.Load<Sprite>("Images/Icons/64px/Weapon_Bow");
        }
        else if (iconType == IconTypeEnum.Campfire)
        {
            iconForeground.sprite = Resources.Load<Sprite>("Images/Icons/64px/Elements_Fire");
        }


        iconDenied.enabled = !clickable;
    }

    void toggleVisible()
    {
        toggleVisible(!visible);
    }

    public void toggleVisible(bool _visible)
    {
        if (visible != _visible)
        {
            visible = _visible;
            iconForeground.enabled = visible;
            iconBackground.enabled = visible;
            if (iconDenied != null)
            {
                iconDenied.enabled = visible && !isClickableDelegate();
            }
        }        
    }

}
