﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;



public class IconSpawner : MonoBehaviour
{
    static GameObject iconGO;

    void Awake()
    {
        iconGO = (GameObject)Resources.Load("Prefabs/UI/Icon");
    }

    public static Icon spawnIcon()
    {
        Icon icon;

        Transform iconTransform;
        iconTransform = Instantiate(iconGO.transform) as Transform;
        iconTransform.SetParent(GameObject.Find("Canvas").transform, false);

        icon = iconTransform.GetComponent<Icon>();

        return icon;
    }
}