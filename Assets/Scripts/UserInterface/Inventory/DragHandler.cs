﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public delegate void OnDragSuccessDelegate();

public class DragHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {


    public OnDragSuccessDelegate onDragSuccessDelegate;

    public static GameObject itemBeingDragged;
    Vector3 startPosition;
    public Transform startParent;
    Transform canvas;

    GameObject itemBeingSplit;

    void Start()
    {
        canvas = GameObject.Find("InventoryCanvas").transform;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        //mwwhahah do i take you or half of youu
        if (Input.GetMouseButton(1))
        {
            itemBeingSplit = (GameObject)Instantiate(gameObject, transform.position, transform.rotation);
            itemBeingSplit.transform.SetParent(transform.parent);
            //itemBeingSplit.GetComponent<InventoryButton>().inventoryItem.quantity = itemBeingSplit.GetComponent<InventoryButton>().inventoryItem.quantity / 2;
            //GetComponent<InventoryButton>().inventoryItem.quantity = GetComponent<InventoryButton>().inventoryItem.quantity / 2;
        }


        itemBeingDragged = gameObject;
        startPosition = transform.position;
        startParent = transform.parent;
        GetComponent<CanvasGroup>().blocksRaycasts = false;
        transform.parent = canvas;
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        itemBeingDragged = null;
        GetComponent<CanvasGroup>().blocksRaycasts = true;
        if (transform.parent != startParent && transform.parent != canvas)
        {
            transform.position = transform.parent.position;
            startParent = transform.parent;

            if (onDragSuccessDelegate != null)
                onDragSuccessDelegate();
        }
        else
        {
            transform.position = startPosition;
            Destroy(itemBeingSplit);
        }

        itemBeingSplit = null;
    }
}
