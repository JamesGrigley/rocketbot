﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class DragHandler2 : StandaloneInputModule
{
    [SerializeField]
    private bool enablePointerHoldEvent = true;

    // called each tick on active input module
    public override void Process()
    {
        base.Process();

        if (enablePointerHoldEvent)
        {
            bool leftIsPressed = Input.GetMouseButton(0);
            if (leftIsPressed)
            {
                MouseState mouseState = GetMousePointerEventData();
                MouseButtonEventData leftButtonData = mouseState.GetButtonState(PointerEventData.InputButton.Left).eventData;
                ProcessPointerHoldEvent(leftButtonData);
            }
        }
    }

    private void ProcessPointerHoldEvent(MouseButtonEventData data)
    {
        var pointerEvent = data.buttonData;

        if (pointerEvent.dragging)
        {
            return;
        }

        var currentOverGob = pointerEvent.pointerCurrentRaycast.gameObject;

        Debug.Log("Calling Execute: " + currentOverGob.name);
        //ExecuteEvents.Execute(currentOverGob, new PointerHoldEventData(eventSystem, 1.0f), CustomInputEvents.PointerHoldEventHandler);   // THIS IS AS FAR AS IT GETS
    }

}
