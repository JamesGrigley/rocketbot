﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class InventoryButton : MonoBehaviour
{
    public delegate void ButtonClickDelegate(InventoryItem item);
    public ButtonClickDelegate buttonClickDelegate;
    public InventoryItem inventoryItem;

    DragHandler dragHandler;

    Button button;
    Text quantityText;
    Image itemImage;

    public int quantity
    {
        get
        {
            return inventoryItem.quantity;
        }
        set
        {
            inventoryItem.quantity = value;
            refreshButton();
        }
    }

    void Awake()
    {
        dragHandler = GetComponent<DragHandler>();
        button = GetComponent<Button>();
        quantityText = transform.GetChild(0).GetComponent<Text>();
        itemImage = GetComponent<Image>();
    }

    public void setButton(InventoryItem _inventoryItem, ButtonClickDelegate _buttonClickDelegate, OnDragSuccessDelegate _onDragSuccessDelegate = null)
    {
        buttonClickDelegate = _buttonClickDelegate;
        inventoryItem = _inventoryItem;
        loadButton();

        if (_onDragSuccessDelegate != null)
        {
            dragHandler.onDragSuccessDelegate = _onDragSuccessDelegate;
        }
    }

    void loadButton()
    {
        itemImage.sprite = inventoryItem.iconSprite;
        itemImage.color = Color.black;
        button.onClick.AddListener(delegate { buttonClickDelegate(inventoryItem); });
        refreshButton();
    }

    void refreshButton()
    {
        if (quantity <= 0)
        {
            Destroy(gameObject);
        }
        else
        {
            if (inventoryItem.quantity > 1)
                quantityText.text = inventoryItem.quantity.ToString();
            else
                quantityText.text = "";
        }
    }
}