﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class InventoryIndicator : MonoBehaviour
{

    Image backgroundImage;
    Image iconImage;
    Text quantityText;

    float fadeSpeed = 0.2f;


    List<Graphic> fadingObjects;

    bool isFading = false;

    void Awake()
    {
        backgroundImage = GetComponent<Image>();
        iconImage = transform.Find("Image").GetComponent<Image>();
        quantityText = transform.Find("Text").GetComponent<Text>();

        fadingObjects = new List<Graphic>();
        fadingObjects.Add(backgroundImage);
        fadingObjects.Add(iconImage);
        fadingObjects.Add(quantityText);
    }

    void Start()
    {
        StartCoroutine(startFading());
    }


    void Update()
    {
        if (isFading)
        {
            foreach (Graphic image in fadingObjects)
            {
                if (image != null)
                {
                    image.CrossFadeAlpha(0, fadeSpeed, false);
                }
            }
        }
    }

    public void setIndicator(ItemTypeEnum itemType, int quantity)
    {
        iconImage.sprite = InventoryUIItem.getIcon(itemType);
        quantityText.text = "+" + quantity.ToString();
    }

    IEnumerator startFading()
    {
        yield return new WaitForSeconds(3);
        isFading = true;
        yield return new WaitForSeconds(0.5f);
        GameObject.Destroy(this.gameObject);

    }


}
