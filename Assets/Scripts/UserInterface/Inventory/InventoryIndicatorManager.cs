﻿using UnityEngine;
using System.Collections;

public class InventoryIndicatorManager : MonoBehaviour
{
    Transform indicatorPrefab;

    float lastIndicator = 0;
    float indicatorDelay = 0.1f;


    // queue them


    void Awake()
    {
        lastIndicator = Time.time;
        indicatorPrefab = Resources.Load<Transform>("Prefabs/UI/InventoryIndicator");
    }

    public void showIndicator(ItemTypeEnum itemType, int quantity)
    {
        StartCoroutine(doShowIndicator(itemType, quantity));
    }

    IEnumerator doShowIndicator(ItemTypeEnum itemType, int quantity)
    {
        float delay = 0;

        if (lastIndicator == 0 || Time.time > lastIndicator)
        {
            lastIndicator = Time.time + indicatorDelay;
        }
        else if (Time.time < lastIndicator)
        {
            delay = lastIndicator - Time.time;
            lastIndicator += indicatorDelay;
        }

        yield return new WaitForSeconds(delay);
        InventoryIndicator indicator = Instantiate(indicatorPrefab).gameObject.GetComponent<InventoryIndicator>();
        indicator.setIndicator(itemType, quantity);
        indicator.transform.parent = transform;
        indicator.transform.localScale = new Vector3(1, 1, 1);

    }
}

