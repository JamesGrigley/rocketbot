﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InventoryUI : MonoBehaviour {

    Inventory inventory;
    List<InventoryUIItem> inventoryItems;
    Transform inventoryUI;
    GameObject inventoryUIItemPrefab;


    // Use this for initialization
    void Start () {
        inventory = GameObject.FindGameObjectWithTag("Player").GetComponent<Inventory>();
        inventoryUI = GameObject.Find("InventoryUI").transform;
        inventoryUIItemPrefab = (GameObject)Resources.Load("Prefabs/UI/InventoryUIItem");

        loadInventoryUI();
    }
	
	void Update () {
        foreach(InventoryUIItem itemUI in inventoryItems)
        {
            InventoryItem item = itemUI.inventoryItem;
            if (inventory.getItem(item.itemType).quantity != item.quantity)
            {
                itemUI.showBling(inventory.getItem(item.itemType).quantity);
            }
            item.quantity = inventory.getItem(item.itemType).quantity;
        }
    }

    void loadInventoryUI()
    {
        inventoryItems = new List<InventoryUIItem>();

        int x = 0;

        addInventoryUIItem(ref x, ItemTypeEnum.Trash);
        addInventoryUIItem(ref x, ItemTypeEnum.Wood);
        addInventoryUIItem(ref x, ItemTypeEnum.Rock);
        addInventoryUIItem(ref x, ItemTypeEnum.Axe);
        addInventoryUIItem(ref x, ItemTypeEnum.Shovel);
        addInventoryUIItem(ref x, ItemTypeEnum.Pickaxe);
        addInventoryUIItem(ref x, ItemTypeEnum.Carrot);

    }

    void addInventoryUIItem(ref int x, ItemTypeEnum itemType)
    {
        int incX = 80;

        // instansiate prefab
        Transform inventoryUIItemTransform;

        inventoryUIItemTransform = Instantiate(inventoryUIItemPrefab.transform) as Transform;

        // make UI canvas the parent
        inventoryUIItemTransform.SetParent(inventoryUI, false);
        inventoryUIItemTransform.localPosition = new Vector3(x, 0, 0);
        InventoryUIItem inventoryUIItem = inventoryUIItemTransform.gameObject.GetComponent<InventoryUIItem>();
        inventoryUIItem.create(itemType);
        inventoryItems.Add(inventoryUIItem);
        x += incX;

    }
}
