﻿
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InventoryUIItem : MonoBehaviour {

    AudioSource audioSource;
    Text blingText;

    Vector3 originalBlingPosition;
    public InventoryItem inventoryItem;


    public Image inventoryIcon;
    Text quantityText;

    public string iconPath;



    bool isBlinging = false;

    void Awake()
    {

        audioSource = GetComponent<AudioSource>();
        blingText = transform.FindChild("Text").GetComponent<Text>();
        originalBlingPosition = blingText.transform.localPosition;
        inventoryIcon = GetComponent<Image>();
        quantityText = transform.FindChild("Image/CountText").GetComponent<Text>();
    }

    void FixedUpdate()
    {
        if (isBlinging)
        {
            blingText.rectTransform.Translate(new Vector3(0, 1, 0));
        }

        if (quantityText != null)
            quantityText.text = inventoryItem.quantity.ToString();
    }

    void refresh()
    {

        inventoryIcon.sprite = getIcon(inventoryItem.itemType);

    }

    public void create(ItemTypeEnum _itemType)
    {
        inventoryItem = new InventoryItem(_itemType, 0);
        refresh();
    }


    public void showBling(int _quantity)
    {
        audioSource.Play(1);

        // instansiate icon
        blingText.transform.localPosition = originalBlingPosition;
        // instansiate bling amount
        int blingAmount = _quantity - inventoryItem.quantity;
        blingText.text = blingAmount > 0 ? "+" + blingAmount.ToString() : blingAmount.ToString();
        blingText.color = blingAmount > 0 ? Color.green : Color.red;
        isBlinging = true;
        StartCoroutine(stopBling());
        // move icon up

        // destroy icon
    }

    IEnumerator stopBling()
    {
        yield return new WaitForSeconds(2);
        isBlinging = false;
        blingText.text = "";
        

    }



    public static Sprite getIcon(ItemTypeEnum _itemType)
    {
        Sprite sprite = new Sprite();

        if (_itemType == ItemTypeEnum.Axe)
        {
            sprite = Resources.Load<Sprite>("Images/Icons/128px/Weapons_Axe");
        }
        else if (_itemType == ItemTypeEnum.Hacksaw)
        {
            sprite = Resources.Load<Sprite>("Images/Icons/128px/Tools_Saw");
        }
        else if (_itemType == ItemTypeEnum.Metal)
        {
            sprite = Resources.Load<Sprite>("Images/Icons/128px/Rewards_Cup");
        }
        else if (_itemType == ItemTypeEnum.Pickaxe)
        {
            sprite = Resources.Load<Sprite>("Images/Icons/128px/Tools_Pick");
        }
        else if (_itemType == ItemTypeEnum.Rock)
        {
            sprite = Resources.Load<Sprite>("Images/Icons/128px/Elements_Ground");
        }
        else if (_itemType == ItemTypeEnum.Trash)
        {
            sprite = Resources.Load<Sprite>("Images/Icons/128px/Inventory_Moneybag");
        }
        else if (_itemType == ItemTypeEnum.Wood)
        {
            sprite = Resources.Load<Sprite>("Images/Icons/128px/Inventory_Wood");
        }
        else if (_itemType == ItemTypeEnum.Carrot)
        {
            sprite = Resources.Load<Sprite>("Images/Icons/128px/Inventory_Carrot");
        }
        else if (_itemType == ItemTypeEnum.Shovel)
        {
            sprite = Resources.Load<Sprite>("Images/Icons/128px/Tools_Shovel");
        }
        else if (_itemType == ItemTypeEnum.FishRaw)
        {
            sprite = Resources.Load<Sprite>("Images/Icons/128px/Inventory_RawFish");
        }
        else if (_itemType == ItemTypeEnum.FishCooked)
        {
            sprite = Resources.Load<Sprite>("Images/Icons/128px/Inventory_CookedFish");
        }
        else if (_itemType == ItemTypeEnum.CarrotCooked)
        {
            sprite = Resources.Load<Sprite>("Images/Icons/128px/Inventory_CarrotCooked");
        }
        else if (_itemType == ItemTypeEnum.Bandage)
        {
            sprite = Resources.Load<Sprite>("Images/Icons/128px/Bandage");
        }
        return sprite;
    }

}
