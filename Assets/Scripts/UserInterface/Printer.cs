﻿using UnityEngine;
using System.Collections;

public class Printer : BaseContainer
{

    PrinterWindow printerWindow;

    void Awake()
    {
        printerWindow = GetComponent<PrinterWindow>();

        if (iconType.ToString() == "")
            iconType = IconTypeEnum.Inspect;
        //activationTime = 1;
        base.Awake();
    }

    public override void openContainer()
    {
        printerWindow.activate();
    }

}
