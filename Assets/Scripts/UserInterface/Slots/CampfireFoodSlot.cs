﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class CampfireFoodSlot : Slot
{
    CookingSlots cookingSlots;

    void Awake()
    {
        cookingSlots = transform.parent.Find("CookingSlots").GetComponent<CookingSlots>();
    }

    protected override bool canDrop(GameObject objectBeingDropped)
    {
        bool canDropReturn = false;

        InventoryButton inventoryButton = objectBeingDropped.GetComponent<InventoryButton>();
        if (inventoryButton.inventoryItem.isCookable)
        {
            return true;
        }

        return canDropReturn;
    }

    protected override void afterDrop()
    {
        Debug.Log("After drop");
        int quantity = inventoryButton.quantity;
        inventoryButton.quantity = 0;

        inventoryButton.inventoryItem.quantity = 1;
        for (int x = 0; x < quantity; x++)
        {
            cookingSlots.addSlot(inventoryButton.inventoryItem);
        }

        //Destroy(item);

    }

}
