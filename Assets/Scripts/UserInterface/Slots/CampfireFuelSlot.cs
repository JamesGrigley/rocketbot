﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class CampfireFuelSlot : Slot
{
    FuelSlots fuelSlots;

    void Awake()
    {
        fuelSlots = transform.parent.Find("FuelSlots").GetComponent<FuelSlots>();
    }

    protected override bool canDrop(GameObject objectBeingDropped)
    {
        bool canDropReturn = false;

        InventoryButton inventoryButton = objectBeingDropped.GetComponent<InventoryButton>();
        if(inventoryButton.inventoryItem.isBurnable)
        {
            return true;
        }

        return canDropReturn;
    }

    protected override void afterDrop()
    {
        Debug.Log("After drop");
        int quantity = inventoryButton.quantity;
        inventoryButton.quantity = 0;

        inventoryButton.inventoryItem.quantity = 1;
        for (int x = 0; x < quantity; x++)
        {
            fuelSlots.addSlot(inventoryButton.inventoryItem);
        }
    }
}
