﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class CookingSlot : Slot
{
    Text timeLeftText;

    FuelSlots fuelSlots;

    float secondsToCook = 20;
    float secondsRemaining;

    bool isCooked = false;

    void Awake()
    {
        fuelSlots = GameObject.Find("FuelSlots").GetComponent<FuelSlots>();
        secondsRemaining = secondsToCook;
        timeLeftText = transform.Find("Text").GetComponent<Text>();
        timeLeftText.text = secondsToTime(secondsRemaining);
    }

    void Update()
    {
        if (!isCooked && fuelSlots.isBurning)
        {
            cook();
        }

        if (secondsRemaining == 0 && !isCooked)
        {
            isCooked = true;
            finishCooking();
        }
    }

    void cook()
    {
        secondsRemaining = Mathf.Clamp(secondsRemaining - Time.deltaTime, 0, float.MaxValue);

        timeLeftText.text = secondsToTime(secondsRemaining);

    }

    string secondsToTime(float _secondsToConvert)
    {
        int seconds = Mathf.FloorToInt(_secondsToConvert % 60);
        int minutes = Mathf.FloorToInt(_secondsToConvert / 60);
        string time = minutes.ToString() + ":" + seconds.ToString("00");
        return time;
    }

    void finishCooking()
    {
        if (inventoryButton.inventoryItem.itemType == ItemTypeEnum.FishRaw)
        {
            inventoryButton.setButton(new InventoryItem(ItemTypeEnum.FishCooked, 1), buttonClick);
        }
        else if (inventoryButton.inventoryItem.itemType == ItemTypeEnum.Carrot)
        {
            inventoryButton.setButton(new InventoryItem(ItemTypeEnum.CarrotCooked, 1), buttonClick);
        }
        timeLeftText.text = "DONE";

    }

    protected override bool canDrop(GameObject objectBeingDropped)
    {
        return false;
    }

    public void buttonClick(InventoryItem _item)
    {
        Debug.Log("I clicked " + _item.itemType.ToString());
    }
}
