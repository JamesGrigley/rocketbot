﻿using UnityEngine;
using System.Collections;

public class CookingSlots : MonoBehaviour
{
    Transform cookingSlotPrefab;
    Transform inventoryItemPrefab;

    void Awake()
    {
        cookingSlotPrefab = Resources.Load<Transform>("Prefabs/UI/CookingSlot");
        inventoryItemPrefab = Resources.Load<Transform>("Prefabs/UI/InventoryButton");

    }

    public void addSlot(InventoryItem item)
    {
        // the item
        Transform itemToAdd = (Transform)Instantiate(inventoryItemPrefab);
        InventoryButton inventoryButton = itemToAdd.GetComponent<InventoryButton>();

        CookingSlot cookingSlot = Instantiate(cookingSlotPrefab).gameObject.GetComponent<CookingSlot>();
        cookingSlot.transform.SetParent(transform);
        //cookingSlot.setSlot(item.itemType);
        itemToAdd.SetParent(cookingSlot.transform);
        //cookingSlot.item = inventoryButton.gameObject;

        inventoryButton.setButton(item, buttonClick, delegate { Destroy(cookingSlot.gameObject); });

    }

    public void buttonClick(InventoryItem _item)
    {
        Debug.Log("Clicked " + _item.itemType.ToString());

    }

}