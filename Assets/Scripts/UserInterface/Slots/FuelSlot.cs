﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FuelSlot : Slot
{
    Text timeLeftText;

    float timeStart;
    float secondsToCook;
    public float secondsRemaining;

    bool isCooked = false;

    void Awake()
    {
        secondsToCook = Random.Range(20, 60);
        timeStart = Time.time;
        secondsRemaining = secondsToCook;
        timeLeftText = transform.Find("Text").GetComponent<Text>();
    }

    void Update()
    {
        if (!isCooked)
        {
            cook();
        }

        if (secondsRemaining == 0 && !isCooked)
        {
            isCooked = true;
            finishCooking();
        }
    }

    void cook()
    {
        secondsRemaining = Mathf.Clamp(secondsToCook - (Time.time - timeStart), 0, float.MaxValue);
        int seconds = Mathf.FloorToInt(secondsRemaining % 60);
        int minutes = Mathf.FloorToInt(secondsRemaining / 60);
        string time = minutes.ToString() + ":" + seconds.ToString("00");

        timeLeftText.text = time;

    }

    void finishCooking()
    {
        transform.parent.GetComponent<FuelSlots>().checkBurn();
        Destroy(gameObject);

    }

    protected override bool canDrop(GameObject objectBeingDropped)
    {
        return false;
    }

    public void buttonClick(InventoryItem _item)
    {
        Debug.Log("I clicked " + _item.itemType.ToString());
    }
}
