﻿using UnityEngine;
using System.Collections;

public class FuelSlots : MonoBehaviour
{
    Transform fuelSlotPrefab;
    Transform inventoryItemPrefab;
    public Campfire campfire;

    public bool isBurning = false;

    void Awake()
    {
        fuelSlotPrefab = Resources.Load<Transform>("Prefabs/UI/FuelSlot");
        inventoryItemPrefab = Resources.Load<Transform>("Prefabs/UI/InventoryButton");
    }

    public void addSlot(InventoryItem item)
    {
        // the item
        Transform itemToAdd = (Transform)Instantiate(inventoryItemPrefab);
        InventoryButton inventoryButton = itemToAdd.GetComponent<InventoryButton>();

        FuelSlot fuelSlot = Instantiate(fuelSlotPrefab).gameObject.GetComponent<FuelSlot>();
        fuelSlot.transform.SetParent(transform);
        itemToAdd.SetParent(fuelSlot.transform);

        inventoryButton.setButton(item, buttonClick, delegate { Destroy(fuelSlot.gameObject); });

        checkBurn();
    }

    public void buttonClick(InventoryItem _item)
    {
        Debug.Log("Clicked " + _item.itemType.ToString());

    }

    public void checkBurn()
    {
        bool checkBurn = false;

        foreach(FuelSlot fuelSlot in transform.GetComponentsInChildren<FuelSlot>())
        {
            if (fuelSlot.secondsRemaining > 0)
            {
                checkBurn = true;
                break;
            }
        }

        isBurning = checkBurn;

        campfire.toggleFire(isBurning);
    }
}
