﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public abstract class Slot : MonoBehaviour, IDropHandler {

    public InventoryButton inventoryButton
    {
        get
        {
            return gameObject.GetComponentInChildren<InventoryButton>();
        }
    }

	public GameObject item
    {
        get
        {
            if (gameObject.GetComponentInChildren<InventoryButton>() != null)
            {
                return gameObject.GetComponentInChildren<InventoryButton>().gameObject;
            }
            else
            {
                return null;
            }

        }
        set
        {

            value.transform.parent = transform;
        }
    }

    public void removeButton()
    {
        Destroy(transform.GetChild(0).gameObject);
    }

    public void OnDrop(PointerEventData eventData)
    {
        if (canDrop(DragHandler.itemBeingDragged))
        {
            InventoryButton draggingInventoryButton = DragHandler.itemBeingDragged.GetComponent<InventoryButton>();

            if (!item)
            {
                // just place item
                DragHandler.itemBeingDragged.transform.SetParent(transform);
            }
            //else if (item.GetComponent<InventoryButton>().inventoryItem.itemType == draggingInventoryButton.inventoryItem.itemType)
            //{
            //    inventoryButton.inventoryItem.quantity += draggingInventoryButton.inventoryItem.quantity;
            //    //Destroy(DragHandler.itemBeingDragged);
            //}
            else
            {
                // swap item
                DragHandler itemInSlot = item.GetComponent<DragHandler>();
                itemInSlot.transform.SetParent(DragHandler.itemBeingDragged.GetComponent<DragHandler>().startParent);
                DragHandler.itemBeingDragged.transform.SetParent(transform);
                
            }
            afterDrop();
        }
    }

    protected virtual bool canDrop(GameObject objectBeingDropped) { return true; }

    protected virtual void afterDrop() { }
}
