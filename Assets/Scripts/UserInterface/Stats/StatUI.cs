﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StatUI : MonoBehaviour
{
    Image backgroundImage;
    //Text statLabelText;
    Text statusLabelText;
    Text futureLabeltext;

    void Awake()
    {
        backgroundImage = transform.Find("Background").GetComponent<Image>();
        //statLabelText = transform.Find("StatLabel").GetComponent<Text>());
        statusLabelText = transform.Find("StatStatus").GetComponent<Text>();
        futureLabeltext = transform.Find("StatFuture").GetComponent<Text>();
    }

    public void setStat(string status, string future, Color color)
    {
        statusLabelText.text = status;
        futureLabeltext.text = future;
        backgroundImage.color = color;

    }
}