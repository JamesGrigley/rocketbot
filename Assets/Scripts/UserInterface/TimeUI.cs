﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TimeUI : MonoBehaviour {

    Sky sky;
    Text dateTimeText;

	// Use this for initialization
	void Start () {
        sky = GameObject.Find("Sky").GetComponent<Sky>();
        dateTimeText = GameObject.Find("DateTimeText").GetComponent<Text>();
    }
	
	// Update is called once per frame
	void Update () {
        dateTimeText.text = "Day " + sky.currentDay.ToString() + "     " + string.Format("{0:00}:{1:00}", sky.worldTimeHour, sky.worldTimeMinute); 
    }
}