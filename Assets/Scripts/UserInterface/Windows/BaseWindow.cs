﻿using UnityEngine;
using System.Collections;


public abstract class BaseWindow : MonoBehaviour {

    Rect windowRect;

    private int windowWidth = 600;
    private int windowHeight = 400;

    public string windowTitle = "";

    protected bool showWindow = false;

    AudioSource audioSource;

    protected GUISkin guiSkin;

    public bool activateOnLoad = false;

    public int WindowWidth
    {
        get
        {
            return windowWidth;
        }
    }

    public int WindowHeight
    {
        get
        {
            return windowHeight;
        }
    }

    public void Awake()
    {
        windowRect = new Rect((Screen.width - windowWidth) / 2, (Screen.height - windowHeight) / 2, windowWidth, windowHeight);
        audioSource = GetComponent<AudioSource>();
        guiSkin = (GUISkin)Resources.Load("Styles/WindowSkin");
        //GameObject framePrefab = (GameObject)Resources.Load("Prefabs/WindowFrame");
        //SpriteRenderer spriteRenderer = framePrefab.GetComponent < SpriteRenderer >();
        //guiSkin.window.normal.background = spriteRenderer.sprite.texture;
    }

    public void Start()
    {
        if (activateOnLoad)
            activate();
    }

    void Update()
    {
        if (showWindow && (Input.GetKeyDown(KeyCode.Escape) || Input.GetMouseButtonDown(1)))
        {
            toggleWindow();
        }
    }

    public virtual void activate()
    {
        toggleWindow();
    }

    void OnGUI()
    {
        GUI.skin = guiSkin;

        if (showWindow)
        {
            windowRect = GUI.Window(0, windowRect, drawWindowContents, windowTitle);
        }
    }

    protected virtual void drawWindowContents(int windowID)
    {
        drawWindow();
        drawCloseButton();
    }

    protected abstract void drawWindow();

    protected void drawCloseButton()
    {
        GUILayout.FlexibleSpace();
        GUIHelper.drawCenteredButton("Close Window", toggleWindow);
    }

    protected void toggleWindow()
    {
        showWindow = !showWindow;
        if (showWindow)
        {
            Time.timeScale = 0;
            if (audioSource != null)
                audioSource.Play();
        }
        else
        {
            Time.timeScale = 1;
        }
    }
}
