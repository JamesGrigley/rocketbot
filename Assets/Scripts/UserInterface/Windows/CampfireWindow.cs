﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class CampfireWindow : MonoBehaviour {

    bool showingWindow = false;

    InventoryWindow inventoryWindow;
    CanvasGroup canvasGroup;
    FuelSlots fuelSlots;

    void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }

    void Start()
    {
        inventoryWindow = GameObject.Find("InventoryCanvas").GetComponent<InventoryWindow>();
        fuelSlots = GameObject.Find("FuelSlots").GetComponent<FuelSlots>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab) || Input.GetKeyDown(KeyCode.I) || Input.GetKeyDown(KeyCode.Escape))
        {
            if (showingWindow) // only close this window with key
                toggleWindow(null);
        }
    }

    public void toggleWindow(bool _showingWindow)
    {
        showingWindow = !_showingWindow;
        toggleWindow(null);
    }

    public void toggleWindow(Campfire _campfire)
    { 
        if (_campfire != null)
            fuelSlots.campfire = _campfire;

        showingWindow = !showingWindow;
        inventoryWindow.toggleWindow(showingWindow);
        WindowManager.isWindowPopped = showingWindow;


        canvasGroup.alpha = showingWindow ? 1 : 0;
        canvasGroup.blocksRaycasts = showingWindow ? true : false;
        canvasGroup.interactable = showingWindow ? true : false;
    }


}
