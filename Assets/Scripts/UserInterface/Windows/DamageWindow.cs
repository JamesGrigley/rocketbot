﻿using UnityEngine;
using System.Collections;

public class DamageWindow : BaseWindow {

    BodyPart bodyPart;

    void Start()
    {
        bodyPart = GetComponent<BodyPart>();
        base.Start();
    }

    public void activate(BodyPart _bodyPart)
    {
        bodyPart = _bodyPart;
        activate();
    }


    override protected void drawWindow()
    {
        GUIHelper.drawCenteredLabel(bodyPart.bodyPartType.ToString() + " current damage is: " + bodyPart.damageState);
    }
}
