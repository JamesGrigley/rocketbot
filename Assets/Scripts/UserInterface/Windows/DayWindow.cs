﻿using UnityEngine;
using System.Collections;

public class DayWindow : BaseWindow {

    Sky sky;

    int lastDayPopped = 1;


    void Awake()
    {
        sky = GameObject.Find("Sky").GetComponent<Sky>();
        base.Awake();
    }

	void Update () {
	    if(sky.currentDay > lastDayPopped)
        {
            lastDayPopped = sky.currentDay;
            toggleWindow();
        }
	}

    override protected void drawWindow()
    {
        GUIHelper.drawCenteredLabel("Welcome to Day " + sky.currentDay.ToString());
    }

}
