﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class InventoryWindow : MonoBehaviour {

    Inventory inventory;
    Transform inventoryItemPrefab;

    bool showingWindow = false;
    Transform inventoryWindow;

    Slot[] slots;

    Player player;
    CanvasGroup canvasGroup;

    StatsWindow statsWindow;

    void Awake()
    {
        canvasGroup = transform.Find("InventoryWindow").GetComponent<CanvasGroup>();
        statsWindow = canvasGroup.transform.Find("PlayerStats").GetComponent<StatsWindow>();
    }

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        inventoryItemPrefab = Resources.Load<Transform>("Prefabs/UI/InventoryButton");
        inventoryWindow = transform.GetChild(0);
        inventoryWindow.gameObject.SetActive(false);
        inventory = GameObject.FindGameObjectWithTag("Player").GetComponent<Inventory>();
        inventory.inventoryUpdatedDelegate = inventoryUpdated;
        slots = inventoryWindow.GetComponentsInChildren<Slot>();
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Tab) || Input.GetKeyDown(KeyCode.I) || Input.GetKeyDown(KeyCode.Escape))
        {
            toggleWindow();
        }
    }

    public void inventoryUpdated(InventoryItem item)
    {
        if (!updateSlot(item))
        {
            addSlot(item);
        }
    }

    void addSlot(InventoryItem item)
    {
        // the item
        Transform itemToAdd = (Transform)Instantiate(inventoryItemPrefab);
        InventoryButton inventoryButton = itemToAdd.GetComponent<InventoryButton>();
        inventoryButton.setButton(item, buttonClick);


        // the slot
        Slot emptySlot = getFirstEmptySlot();
        emptySlot.item = inventoryButton.gameObject;
    }

    bool updateSlot(InventoryItem item)
    {
        foreach (Slot slot in slots)
        {
            if (slot.inventoryButton != null)
            {
                try
                {
                    if (slot.inventoryButton.inventoryItem.itemType == item.itemType)
                    {
                        slot.inventoryButton.setButton(item, buttonClick);

                        return true;
                    }
                }
                catch (System.Exception ex)
                {
                    Debug.Log("slot.inventoryButton.inventoryItem" + (slot.inventoryButton.inventoryItem == null ? "null" : "not null"));
                    Debug.Log("item" + (item == null ? "null" : "not null"));

                }


            }
        }
        return false;
    }

    Slot getFirstEmptySlot()
    {
        foreach(Slot slot in slots)
        {
            if(slot.item == null)
            {
                return slot;
            }
        }
        return null;
    }

    public void toggleWindow(bool visible)
    {
        showingWindow = !visible; // flip it because toggle will flip again
        toggleWindow();
    }


    public void toggleWindow()
    {
        showingWindow = !showingWindow;
        //Time.timeScale = showingWindow ? 0 : 1;

        canvasGroup.alpha = showingWindow ? 1 : 0;
        canvasGroup.blocksRaycasts = showingWindow ? true : false;
        canvasGroup.interactable = showingWindow ? true : false;

        if (showingWindow)
        {
            statsWindow.refresh();
        }

        WindowManager.isWindowPopped = showingWindow;


        inventoryWindow.gameObject.SetActive(showingWindow);
    }

    public void buttonClick(InventoryItem _item)
    {
        consumeItem(_item);
    }

    void consumeItem(InventoryItem item)
    {
        Debug.Log("Consuming " + item.itemType.ToString());

        if (item.itemType == ItemTypeEnum.Carrot || item.itemType == ItemTypeEnum.FishRaw)
        {
            if (player.eat(10))
            {
                item.quantity -= 1;
                updateSlot(item);
            }
        }
        else if (item.itemType == ItemTypeEnum.Bandage)
        {
            if (player.heal())
            {
                item.quantity -= 1;
                updateSlot(item);
            }
        }

        statsWindow.refresh();
    }
}
