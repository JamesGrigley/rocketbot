﻿using UnityEngine;
using System.Collections;

public class MapLocation 
{
    public string locationName;
    public Vector2 mapCoordinates;
    public float fuelRequired;
    public bool isHome = false;
    public string sceneName;

    public MapLocation() { }



    public MapLocation(string _locationName, Vector2 _mapCoordinates, string _sceneName)
    {
        locationName = _locationName;
        mapCoordinates = _mapCoordinates;
        sceneName = _sceneName;
    }
}