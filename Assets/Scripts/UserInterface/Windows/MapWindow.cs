﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System;

public class MapWindow : BaseWindow
{
    public Texture locationTexture;
    public Texture map;
    public Texture backspace;

    public delegate void MapNodeSelectedDelegate();
    public MapNodeSelectedDelegate mapNodeSelectedDelegate;

    List<MapLocation> mapLocations;
    MapLocation currentLocation;

    Ship ship;
    ShipRoutines shipRoutines;

    void Awake()
    {
        ship = GetComponent<Ship>();
        shipRoutines = GetComponent<ShipRoutines>();

        mapLocations = new List<MapLocation>();

        mapLocations.Add(new MapLocation("Gloucester City, NJ", new Vector2(74, 110), "Game"));
        mapLocations.Add(new MapLocation("Brooklawn, NJ", new Vector2(100, 230), "MapLocation2"));
        mapLocations.Add(new MapLocation("Parachute, CO", new Vector2(300, 170), "MapLocation1"));

        MapLocation homeLocation = new MapLocation("HOME", new Vector2(200, 300), "MapLocation1");
        homeLocation.isHome = true;
        homeLocation.fuelRequired = 100;
        mapLocations.Add(homeLocation);

        currentLocation = mapLocations[0];
        refreshMapNodes();
        base.Awake();
    }

    override protected void drawWindow()
    {
        //GUI.DrawTexture(new Rect(0, 0, WindowWidth, WindowHeight), map);
        GUILayout.Label("Current Fuel: " + ship.currentFuel.ToString("0.0"));

        GUILayout.Box(map);
        foreach (MapLocation mapLocation in mapLocations)
        {
            GUILayout.BeginArea(new Rect(mapLocation.mapCoordinates, new Vector2(220, 50)), backspace);
            GUILayout.BeginHorizontal();

            GUIHelper.drawButton(locationTexture, selectMapNode, mapLocation, new Vector2(50, 50));

            string locationText = mapLocation.locationName + "\nFuel Required: " + mapLocation.fuelRequired.ToString("0.0");
            if (mapLocation == currentLocation)
                locationText += "\n\nYOU ARE HERE";

            GUILayout.Label(locationText);
            GUILayout.EndHorizontal();
            GUILayout.EndArea();
        }
        //GUILayout.EndArea();

    }

    private void selectMapNode(System.Object parm)
    {
        selectMapNode((MapLocation)parm);
    }

    public void selectMapNode(MapLocation mapLocation)
    {
        if (mapLocation == currentLocation)
            return;

        if (ship.currentFuel < mapLocation.fuelRequired)
        {
            return;
        }

        Debug.Log("Let go to " + mapLocation.locationName + "!");

        ship.currentFuel -= mapLocation.fuelRequired;
        toggleWindow();

        refreshMapNodes();

        if (mapNodeSelectedDelegate != null)
            mapNodeSelectedDelegate();

        shipRoutines.runRoutine();
        StartCoroutine(changeScene(mapLocation));
    }

    IEnumerator changeScene(MapLocation mapLocation)
    {
        yield return new WaitForSeconds(12);
        SceneManager.LoadScene(mapLocation.sceneName);
    }

    void refreshMapNodes()
    {
        foreach (MapLocation mapLocation in mapLocations)
        {
            if (!mapLocation.isHome)
            {
                mapLocation.fuelRequired = Vector2.Distance(mapLocation.mapCoordinates, currentLocation.mapCoordinates) * 0.1f;
            }
        }
    }
}
