﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PrinterWindow : BaseWindow
{

    Inventory playerInventory;
    Inventory shipInventory;

    //GUIStyle centeredTextStyle;

    public Texture canBackground;
    public Texture canForeground;

    public Texture axeTexture;
    public Texture pickaxeTexture;
    public Texture shovelTexture;
    public Texture hacksawTexture;
    public Texture bandageTexture;

    // Use this for initialization
    void Awake()
    {

        // copy the "label" style from the current skin
        //centeredTextStyle = new GUIStyle(guiSkin.GetStyle("Label"));
        //centeredTextStyle.alignment = TextAnchor.MiddleCenter;

        base.Awake();
    }

    void Start()
    {
        playerInventory = GameObject.FindGameObjectWithTag("Player").GetComponent<Inventory>();
        shipInventory = GameObject.Find("Ship").GetComponent<Inventory>();

        shipInventory.addItem(ItemTypeEnum.Trash, 0);
        shipInventory.addItem(ItemTypeEnum.Rock, 0);
        shipInventory.addItem(ItemTypeEnum.Wood, 0);
        shipInventory.addItem(ItemTypeEnum.Metal, 0);

        base.Start();
    }

    override public void activate()
    {
        unloadPlayerInventory();
        base.activate();
    }

    override protected void drawWindow()
    {

        GUILayout.BeginHorizontal();
        GUILayout.BeginVertical();
        foreach(InventoryItem item in shipInventory.items)
        {
            drawShipInventoryItem(item);
        }
        GUILayout.EndVertical();
        GUILayout.BeginVertical();

        GUIHelper.drawCenteredButton(axeTexture, craftAxe, 50, 50);
        GUIHelper.drawCenteredButton(shovelTexture, craftShovel, 50, 50);
        GUIHelper.drawCenteredButton(pickaxeTexture, craftPickaxe, 50, 50);
        GUIHelper.drawCenteredButton(bandageTexture, craftBandage, 50, 50);

        GUILayout.FlexibleSpace();
        GUILayout.EndVertical();
        GUILayout.EndHorizontal();

    }

    void craftAxe()
    {
        if (shipInventory.getItem(ItemTypeEnum.Trash).quantity >= 1)
        {
            shipInventory.addItem(ItemTypeEnum.Trash, -1);
            playerInventory.addItem(ItemTypeEnum.Axe, 1);
        }
    }

    void craftPickaxe()
    {
        if (shipInventory.getItem(ItemTypeEnum.Trash).quantity >= 1)
        {
            shipInventory.addItem(ItemTypeEnum.Trash, -1);
            playerInventory.addItem(ItemTypeEnum.Pickaxe, 1);
        }
    }

    void craftShovel()
    {
        if (shipInventory.getItem(ItemTypeEnum.Trash).quantity >= 1)
        {
            shipInventory.addItem(ItemTypeEnum.Trash, -1);
            playerInventory.addItem(ItemTypeEnum.Shovel, 1);
        }
    }

    void craftBandage()
    {
        if (shipInventory.getItem(ItemTypeEnum.Trash).quantity >= 1)
        {
            shipInventory.addItem(ItemTypeEnum.Trash, -1);
            playerInventory.addItem(ItemTypeEnum.Bandage, 1);
        }
    }


    void drawShipInventoryItem(InventoryItem item)
    {
        GUILayout.BeginHorizontal();
        GUILayout.Label(item.iconSprite.texture, GUILayout.Width(50), GUILayout.Height(50));
        GUILayout.BeginVertical();
        GUILayout.FlexibleSpace();
        GUILayout.Label(item.quantity.ToString());
        GUILayout.FlexibleSpace();
        GUILayout.EndVertical();
        GUILayout.EndHorizontal();
    }

    //void drawCan(Vector2 position, InventoryItem item)
    //{
    //    GUILayout.BeginVertical();

    //    GUILayout.Label(canBackground);
    //    // background
    //    Rect rect = new Rect(position, new Vector2(100, 50));
    //    GUI.DrawTexture(rect, canBackground);

    //    //foreground
    //    float fillAmount =  item.quantity * 10;
    //    rect.size = new Vector2(rect.size.x, fillAmount);
    //    rect.position = new Vector2(position.x, position.y + 100 - fillAmount);
    //    GUI.DrawTexture(rect, canForeground);

    //    // count
    //    rect.position = position + Vector2.up * 100;
    //    rect.size = new Vector2(rect.size.x, 30);
    //    GUI.Label(rect, item.quantity.ToString());

    //    // name
    //    rect.position = position + Vector2.up * 130;
    //    rect.size = new Vector2(rect.size.x, 30);
    //    GUI.Label(rect, item.itemType.ToString());
    //    GUILayout.EndVertical();

    //}

    void unloadPlayerInventory()
    {
        foreach(InventoryItem item in playerInventory.getResources())
        {
            shipInventory.addItem(item.itemType, item.quantity);
            item.quantity = 0;
        }
    }
}
