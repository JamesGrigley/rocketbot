﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RepairWindow : BaseWindow {

    GUIStyle style;

    public List<InventoryItem> repairRequiredItems;

    Inventory inventory;

    public delegate void StartRepairDelegate();
    public StartRepairDelegate startRepairDelegate;

    void Awake()
    {
        inventory = GameObject.FindGameObjectWithTag("Player").GetComponent<Inventory>();

        style = new GUIStyle();
        style.alignment = TextAnchor.MiddleCenter;
        style.fontSize = 40;

        repairRequiredItems = new List<InventoryItem>();
        //repairRequiredItems.Add(new InventoryItem(ItemTypeEnum.Trash, 1));
        //repairRequiredItems.Add(new InventoryItem(ItemTypeEnum.Wood, 1));
        base.Awake();
    }

    override protected void drawWindow()
    {
        GUILayout.FlexibleSpace();

        GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            foreach (InventoryItem item in repairRequiredItems)
            {
                GUILayout.Label(InventoryUIItem.getIcon(item.itemType).texture);
            }
            GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
        GUILayout.FlexibleSpace();

        GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();

            if (GUILayout.Button("Repair"))
            {
                if (inventory.hasItems(repairRequiredItems))
                {
                    inventory.consumeItems(repairRequiredItems);
                    toggleWindow();
                    startRepairDelegate();
                }
                else
                {
                    Debug.Log("Nope");
                }
            }
            GUILayout.FlexibleSpace();

        GUILayout.EndHorizontal();

        drawCloseButton();
    }

}
