﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShipWindow : BaseWindow {

    Ship ship;
    MapWindow mapWindow;
    RepairWindow repairWindow;
    PrinterWindow printerWindow;

    void Awake()
    {
        ship = GetComponent<Ship>();
        mapWindow = GetComponent<MapWindow>();
        repairWindow = GetComponent<RepairWindow>();
        repairWindow.startRepairDelegate = ship.startRepair;
        printerWindow = GetComponent<PrinterWindow>();
        base.Awake();
    }

    void Start()
    {
        repairWindow.repairRequiredItems.Add(new InventoryItem(ItemTypeEnum.Trash, 1));
        base.Start();
    }

    override protected void drawWindow()
    {
        if (ship.isDamaged)
        {
            GUIHelper.drawCenteredLabel("WARNING! WARNING! Critical damage. Repairs required. CODE 5017");
            GUIHelper.drawCenteredButton("Repair Panel", openRepairWindow);
        }
        else
        {
            GUIHelper.drawCenteredLabel("Scaceship assistant, at your service.");
            GUIHelper.drawCenteredButtons(new List<GUIHelper.button> {
                new GUIHelper.button("3D Printer", openPrinterWindow),
                new GUIHelper.button("Map ", showMap)
            });
        }
    }

    void openRepairWindow()
    {
        toggleWindow();
        repairWindow.activate();
    }

    void openPrinterWindow()
    {
        toggleWindow();
        printerWindow.activate();
    }

    void showMap()
    {
        toggleWindow();
        mapWindow.activate();
    }


}
