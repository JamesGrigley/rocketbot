﻿using UnityEngine;
using System.Collections;

public class StatsWindow : MonoBehaviour
{
    StatUI wellBeingUI;
    StatUI healthUI;
    StatUI hungerUI;

    Health health;
    Hunger hunger;
    Wellbeing wellbeing;

    void Awake()
    {
        wellBeingUI = transform.Find("WellBeing").GetComponent<StatUI>();
        healthUI = transform.Find("Health").GetComponent<StatUI>();
        hungerUI = transform.Find("Hunger").GetComponent<StatUI>();
        health = GameObject.FindGameObjectWithTag("Player").GetComponent<Health>();
        hunger = GameObject.FindGameObjectWithTag("Player").GetComponent<Hunger>();
        wellbeing = GameObject.FindGameObjectWithTag("Player").GetComponent<Wellbeing>();

    }


    void Start()
    {

    }

    public void refresh()
    {
        hungerUI.setStat(hunger.hungerStatusText, hunger.hungerFutureText, hunger.statusColor);

        healthUI.setStat(health.statusText, health.futureText, health.statusColor);

        wellBeingUI.setStat(wellbeing.statusText, wellbeing.futureText, wellbeing.statusColor);
    }
}