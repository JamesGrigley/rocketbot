﻿using UnityEngine;
using System.Collections;

public class VisitorManager : MonoBehaviour
{
    Male maleVisitorPrefab;
    Male maleVisitor;

    Transform spawnPosition;
    
    void Awake()
    {
        maleVisitorPrefab = Resources.Load<Male>("Prefabs/Male1");
        spawnPosition = GameObject.Find("VisitorSpawn").transform;
    } 

    void Start()
    {
        spawnMale();
    }

    void spawnMale()
    {
        maleVisitor = (Male)Instantiate(maleVisitorPrefab, spawnPosition.position, spawnPosition.rotation);
        maleVisitor.gotoAndPerform(GameObject.Find("Door").transform, maleVisitor.knock);
    }

}
