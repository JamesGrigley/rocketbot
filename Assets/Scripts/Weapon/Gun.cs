﻿using UnityEngine;
using System.Collections;

public class Gun : MonoBehaviour
{
    ParticleSystem flareParticle;
    Vector3 originalPosition;
    SoundManager soundManager;

    void Awake()
    {
        flareParticle = GameObject.Find("AKFlare").GetComponent<ParticleSystem>();
        originalPosition = transform.GetChild(0).localPosition;
        soundManager = GameObject.Find("SoundManager").GetComponent<SoundManager>();

    }

    public void shoot()
    {
        soundManager.play(SoundClipTypeEnum.RifleShot, transform);
        flareParticle.Play();
        transform.GetChild(0).localPosition = originalPosition + -Vector3.right * 0.15f;
        StartCoroutine(returnGun());
    }

    IEnumerator returnGun()
    {
        yield return new WaitForSeconds(0.015f);
        transform.GetChild(0).localPosition = originalPosition;
    }
}