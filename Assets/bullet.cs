﻿using UnityEngine;
using System.Collections;

public class bullet : MonoBehaviour {

    Rigidbody rigidbody;
    float maxSpinTorque = 100;

	// Use this for initialization
	void Start () {
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.AddForce(transform.forward * 300);
        rigidbody.AddTorque(new Vector3(Random.Range(-maxSpinTorque, maxSpinTorque), Random.Range(-maxSpinTorque, maxSpinTorque), Random.Range(-maxSpinTorque, maxSpinTorque)));
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
